<?php $options = ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi']; ?>
<div class="form-group row">

    <label for="{{ $name }}" class="col-md-4 control-label">{{ substr($title, 0, 11) }}</label>
    <div class="col-md-6">
    	<select name="{{$name}}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" {{ isset($required) ? 'required' : ''}}>
    		@foreach ($options as $option)
    			@if(isset($value) == $loop->iteration)
	    			<option value="{{ $loop->iteration}}" selected="true">{{ $option }}</option>
    			@else
	    			<option value="{{ $loop->iteration}}">{{ $option }}</option>
    			@endif 
	    	@endforeach
    	</select>
	    @if ($errors->has($name))
	        <div class="invalid-feedback">
	            {{ $errors->first($name) }}
	        </div>
	    @endif    	
    </div>
</div>
