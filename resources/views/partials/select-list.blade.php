<div class="form-group row">
    <label for="{{ $name }}" class="col-md-4 col-xs-12 control-label">{{ $title }}</label>
    <div class="col-md-6 col-xs-12">
    	<select name="{{$name}}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }} {{$name}}" {{ $required ? 'required' : ''}}>
    		@foreach($options as $option)
    			@if(!@empty($value) && ucfirst($option) === ucfirst($value))
	    			<option value="{{ $option }}" selected="true">{{ ucfirst($option) }}</option>
	    		@else
	    			<option value="{{ $option }}">{{ $option }}</option>
	    		@endif
	    	@endforeach
    	</select>
	    @if ($errors->has($name))
	        <div class="invalid-feedback">
	            {{ $errors->first($name) }}
	        </div>
	    @endif    	
    </div>
</div>