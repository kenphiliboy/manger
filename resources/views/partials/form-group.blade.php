<div class="form-group">
    <label for="{{ $name }}" class="col-md-4 col-xs-12 control-label">{{ $title }}</label>
    <div class="col-md-6 col-xs-12">
	    <input id="{{ $name }}" type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" name="{{ $name }}" placeholder="{{ isset($place) ? $place : '' }}" value="{{ old($name, isset($value) ? $value : '') }}" {{ $required ? 'required' : ''}}>
	    @if($errors->has($name))
	        <div class="invalid-feedback">
	            {{ $errors->first($name) }}
	        </div>
	    @endif    	
    </div>
</div>

