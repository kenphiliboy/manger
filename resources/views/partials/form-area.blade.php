<div class="form-group">

    <label for="{{ $name }}" class="col-md-4 col-xs-12">{{ $title }}</label>
    <div class="col-xs-12">
	    <textarea id="{{ $name }}" type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" name="{{ $name }}" placeholder="{{ isset($place) ? $place : '' }}" {{ $required ? 'required' : ''}}
	    	style="height: 125px;">
	    	{{ old($name, isset($value) ? $value : '') }}
	    </textarea>    	
    </div>
    @if ($errors->has($name))
        <div class="invalid-feedback">
            {{ $errors->first($name) }}
        </div>
    @endif
    
</div>
