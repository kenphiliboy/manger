<div class="form-group">
    <label class="col-md-4 control-label">date:</label>
    <div class="col-md-6">
        <div class="input-group" >
            <input type="text" class="form-control {{ $errors->has($name) ? ' is-invalid' : '' }} datepicker" data-date-format="dd-mm-yyyy"  name="{{ isset($name) ? $name : 'pour_le' }}" value="{{ old($name, isset($value) ? $value : '') }}" data-date-viewmode="months" required />
            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
    </div>
</div>

