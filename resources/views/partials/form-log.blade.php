<div class="form-group">
    <input id="{{ $name }}" type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" name="{{ $name }}" placeholder="{{ isset($place) ? $place : '' }}" value="{{ old($name, isset($value) ? $value : '') }}" {{ $required ? 'required' : ''}} >
    @if($errors->has($name))
        <div class="invalid-feedback">
            <li class="msg-feedback">{{ $errors->first($name) }}</li>
        </div>
    @endif
</div>
