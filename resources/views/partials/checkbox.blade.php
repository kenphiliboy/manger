<div class="form-group">
    <label class="{{ isset($class) ? 'col-md-3 col-xs-12 control-label' : '' }}"></label>
    <div class="col-md-6 col-xs-12">          
        <label class="check"><input type="checkbox" class="icheckbox" {{ old('remember') ? 'checked' : '' }} /> @lang($title)</label>
        <span class="help-block">{{ isset($confirm) ? $confirm : '' }}</span>
    </div>
</div>