<form class="horizontal-form" action="{{ $route }}" 	method="post">
	{{ csrf_field() }}
	{{ method_field('DELETE')}}
	<div class="form-group">
		<button class="form-control btn btn-danger">Supprimer</button>
	</div>
</form>