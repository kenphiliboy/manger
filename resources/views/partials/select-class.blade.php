<div class="form-group row">

    <label for="{{ $name }}" class="col-md-4 col-xs-12 control-label">{{ substr($title, 0, 11) }}</label>
    <div class="col-md-6 col-xs-12">
    	<select name="{{$name}}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }} {{$name}}" {{ isset($required) ? 'required' : ''}}>
    		@foreach ($options as $option)
	    			<option value="{{ $option->id }}">{{ $option->ministere }}</option>
    			@if($option->ministere == isset($value))
	    			<option value="{{ $option->id }}" selected="true">{{ $option->ministere }}</option>
	    		@else
	    		@endif
	    	@endforeach
    	</select>
	    @if ($errors->has($name))
	        <div class="invalid-feedback">
	            {{ $errors->first($name) }}
	        </div>
	    @endif    	
    </div>
</div>
