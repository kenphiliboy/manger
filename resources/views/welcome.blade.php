@extends('layouts.form-log')

@section('form')
	@component('components.login')
        @slot('primary')
            @lang('Bienvenue')
        @endslot
        @slot('secondary')
            @lang('Veuillez vous identifier')
        @endslot
		<form method="POST" action="{{ route('admin.log') }}">
            {{ csrf_field() }}
            @include('partials.form-log', [
                'place' => __('Votre eglise profil'),
                'type' => 'text',
                'name' => 'user_login',
                'required' => true,
                ])
            @include('partials.form-log', [
                'place' => __('Mot de passe'),
                'type' => 'password',
                'name' => 'password',
                'required' => true,
                ])
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                <label class="custom-control-label" for="remember"> @lang('Se rappeler de moi')</label>
            </div>
            @component('components.button', [
            	'color'=> 'info'])
                @lang('Connexion')
            @endcomponent
            <a class="btn btn-link pull-left" href="{{ route('password.request') }}">
                @lang('Mot de passe oublié ?')
            </a>
        </form>
    @endcomponent
@endsection
