<div class="login-body">
    <div class="login-title"><strong>{{ $primary }}</strong>,{{ $secondary }}</div>
    {{ $slot }}
</div>