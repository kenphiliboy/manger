<div class="row">
    <div class="col-md-12">       
        <form class="form-horizontal annonce" method="{{ isset($method) ? $method : 'post' }}" action="{{ $route }}" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> {{ $titre }}</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    @yield('champs')
                    @include('partials.form-date',[
                        'name'=> 'pour_le',
                        'value'=> isset($annonce) ? $annonce->pour_le : '' ,
                        ])
                     @include('partials.hidden', [
                        'name' => 'eglise_id',
                        'value' => ''.Auth()->user()->id,
                        ])
                     @include('partials.hidden', [
                        'name' => 'statut',
                        'value' => 0,
                        ])
                </div>
                <div class="panel-footer">
                    @component('components.button')
                        @lang('Envoyer')
                    @endcomponent
                    @component('components.button',[
                        'type' => 'reset',
                        'color' => 'default',
                        'pull' => 'left'])
                        @lang('Effacer')
                    @endcomponent
                </div>
            </div>
        </form>
    </div>
</div>