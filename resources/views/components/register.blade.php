<div class="row">
    <div class="col-md-12">       
        <form class="form-horizontal" method="post" action="{{ $route }}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> {{ $titre1 }}</strong> {{ $titre2 }}</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    @yield('champs')
                </div>
                <div class="panel-footer">
                    @yield('button')                    
                </div>
            </div>
        </form>
    </div>
</div>