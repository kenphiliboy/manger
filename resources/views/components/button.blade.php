<button type="@isset($type){{$type}}@else submit @endisset" class="btn @isset($color){{ ' btn-' . $color }}@else btn-primary @endisset @isset($pull){{ 'pull-' . $pull}} @else pull-right @endisset">
        {{ $slot }}
</button>