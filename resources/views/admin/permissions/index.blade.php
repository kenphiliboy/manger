{{-- \resources\views\permissions\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Permissions')


@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection


@section('content')

<div class="col-lg-8 offset-lg-2 mb-5">
    <h1><i class="fa fa-key"></i>Available Permissions

    <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
    <a href="{{ route('roles.index') }}" class="btn btn-default pull-right">Roles</a></h1>
    <hr>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Table With Full Features</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="permissions" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Permissions</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($permissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td> 
                        <td>
                        <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-info btn-sm pull-left mr-3 invisible" title="@lang('Modifier permission') {{$permission->name}}"><i class="fa fa-edit"></i> edite</a>
                        <a href="{{ route('permissions.destroy', $permission->id) }}" class="btn btn-danger btn-sm invisible" title="@lang('Supprimer permission') {{$permission->name}}">
                            <i class="fa fa-trash fa-lg"></i> supprime</a>  

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <a href="{{ URL::to('permissions/create') }}" class="btn btn-success">Add Permission</a>

</div>

@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script>
        $('#permissions').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $(() => {
            $('a').removeClass('invisible')
        })
        
    </script>
    @include('partials.script-delete', ['text' => __('Vraiment supprimer cette permission ?'), 'return' => 'removeTr'])
@endsection