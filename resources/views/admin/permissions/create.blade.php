{{-- \resources\views\permissions\create.blade.php --}}
@extends('layouts.app')

@section('title', '| Create Permission')

@section('content')
<div class="row">
    <div class="offset-md-3 mt-4 col-md-6">
        <!-- Horizontal Form -->
        <div class="card card-info">
            <div class="card-header">
                <h1 class="card-title"><i class='fa fa-key'></i> Add Permission</h1>
            </div>
            <div class="card-body">
                <br>
                {{ Form::open(array('url' => 'permissions')) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                </div>               
                {{ Form::submit('Ajouter permission', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
