{{-- \resources\views\users\create.blade.php --}}
@extends('layouts.app')

@section('title', '| Add User')

@section('content')
<div class="row">
    <div class="offset-md-3 mt-4 col-md-6">
        <!-- Horizontal Form -->
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-user-plus"></i> Ajouter Utilisateur</h3>
            </div>
            <div class="card-body">
                
                <h1></h1>
                <hr>

                {{ Form::open(array('url' => 'users', 'class'=>'horizontal-form')) }}

                <div class="form-group">
                    {{ Form::label('departement', 'Departement') }}
                    {{ Form::text('departement', '', array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::email('email', '', array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    @foreach ($roles as $role)
                        {{ Form::radio('roles[]',  $role->id ) }}
                        {{ Form::label($role->name, ucfirst($role->name)) }}

                    @endforeach
                </div>

                <div class="form-group">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::password('password', array('class' => 'form-control')) }}

                </div>

                <div class="form-group">
                    {{ Form::label('password', 'Confirm Password') }}
                    {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

                </div>

            </div>

            <div class="card-footer">
                {{ Form::submit('Envoyer', array('class' => 'btn btn-primary float-right')) }}
                {{ Form::reset('Annuler', array('class' => 'btn btn-danger')) }}
            </div>


            {{ Form::close() }}

        </div>
    </div>
</div>
@endsection