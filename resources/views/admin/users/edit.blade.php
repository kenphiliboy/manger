@extends('layouts.app')

@section('title', '| Add User')

@section('content')
<div class="row">
    <div class="offset-md-3 mt-4 col-md-6">
        <!-- Horizontal Form -->
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-user-plus"></i> Edit User {{$user->name}}</h3>
            </div>
            <div class="card-body">
                <hr>

                {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'class'=>'horizontal-form')) }}

                    <div class="form-group">
                        {{ Form::label('departement', 'Name') }}
                        {{ Form::text('departement', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', null, array('class' => 'form-control')) }}
                    </div>

                        <h5><b>Give Role</b></h5>

                    <div class='form-group'>
                        @foreach ($roles as $role)
                            {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                        @endforeach
                    </div>

                </div>

                <div class="card-footer">
                    {{ Form::submit('Envoyer', array('class' => 'btn btn-primary float-right')) }}
                    {{ Form::reset('Annuler', array('class' => 'btn btn-danger')) }}
                </div>


            {{ Form::close() }}

        </div>
    </div>
</div>
@endsection