@extends('layouts.app')

@section('main')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>                    
        <li class="active">Roles</li>
    </ul>
    <!-- END BREADCRUMB --> 
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @component('components.card-form', ['route' => route('annonce.store')])

                @slot('titre')
                    @lang('Creation de role')
                @endslot

                @section('champs')
                    {{ csrf_field() }}            
                    @include('partials.form-group', [
                        'title' => __('Penser de la semaine'),
                        'type' => 'text',
                        'name' => 'pense',
                        'required' => true,
                        ])
                    @include('partials.hidden', [
                        'value' => 'annonce',
                        ])
                @overwrite
            @endcomponent
        </div>
    </div>
@endsection
