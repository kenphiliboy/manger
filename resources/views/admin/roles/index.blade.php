{{-- \resources\views\roles\index.blade.php --}}
@extends('layouts.app')

@section('main')
<!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>                    
        <li class="active">Dashboard</li>
    </ul>
<!-- END BREADCRUMB -->                     
<!-- START WIDGETS -->   

<div class="col-lg-8 col-lg-offset-2 mb-5">
    <h1><i class="fa fa-key"></i> Roles

    <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissions</a></h1>
    <hr>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Table With Full Features</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Role</th>
                        <th>Permissions</th>
                        <th>Operation</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($roles as $role)
                    <tr>

                        <td>{{ $role->name }}</td>

                        <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                        <td>
                        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-info btn-sm pull-left mr-3 invisible" title="@lang('Modifier utilisateur') {{$role->name}}"><i class="fa fa-edit"></i> edite</a>
                        <a href="{{ route('roles.destroy', $role->id) }}" class="btn btn-danger btn-sm invisible" title="@lang('Supprimer ulitisateur') {{$role->name}}">
                            <i class="fa fa-trash fa-lg"></i> supprime</a>   

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <a href="{{ URL::to('roles/create') }}" class="btn btn-success">Add Role</a>

</div>

@endsection


@section('script')
    <!-- DataTables -->
    <script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script> 
    <script>
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $(() => {
            $('a').removeClass('invisible')
        })
        
    </script>
    @include('partials.script-delete', ['text' => __('Vraiment supprimer cette role ?'), 'return' => 'removeTr'])
@endsection