<!DOCTYPE html>
<html lang="{{ 'fr' }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Annonce</title>
    <!-- CSS INCLUDE -->        
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('css/theme-default.css') }}"/>
    @yield('css')
</head>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        
        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <ul class="x-navigation">
                <li class="xn-logo">
                    <a href="{{route('admin.index')}}"></a>
                    <a href="#" class="x-navigation-control"></a>
                </li>
                <li class="xn-profile">
                    <a href="#" class="profile-mini">
                        <img src="{{asset('images/users/avatar.jpg') }}" alt="John Doe"/>
                    </a>
                    <div class="profile">
                        <div class="profile-image">
                            <img src="{{asset('images/users/avatar.jpg') }}" alt="John Doe"/>
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name">John Doe</div>
                            <div class="profile-data-title">Web Developer/Designer</div>
                        </div>
                        <div class="profile-controls">
                            <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                            <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                        </div>
                    </div>                                                                        
                </li>
                <li class="xn-title">Navigation</li>
                @admin
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-users"></span> <span class="xn-text">District</span></a>
                    <ul>
                        <li><a href="{{ route('district.index') }}"><span class="fa fa-wrench"></span> Gerer district</a></li>
                        <li><a href="{{ route('district.create') }}"><span class="fa fa-pencil"></span> Nouvelle district</a></li>
                        <li><a href="#"><span class="fa fa-trash-o"></span> Signaler</a></li>
                    </ul>
                </li>
                <li class="xn-openable">
                    <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Eglise</span></a>
                    <ul>
                        <li><a href="{{ route('eglise.index') }}"><span class="fa fa-wrench"></span> Manager eglise</a></li>
                        <li><a href="{{ route('eglise.create') }}"><span class="fa fa-pencil"></span> Nouvelle eglise</a></li>
                        <li><a href="#"><span class="fa fa-trash-o"></span> Signaler</a></li>
                    </ul>
                </li>
                @endadmin

                @eglise            
                    <li class="active">
                        <a href="{{ route('admin.index') }}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-edit"></span> <span class="xn-text">Annonces</span></a>
                        <ul>
                            <li><a href="{{route('annonce.index')}}"><span class="fa fa-wrench"></span> Gerer Annonce</a></li>
                            <li><a href="{{route('annonce.create')}}"><span class="fa fa-pencil"></span> Nouvelle annonce</a></li>                        
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-suitcase"></span> <span class="xn-text">Groupe</span></a>
                        <ul>
                            <li><a href="{{route('groupe.index')}}"><span class="fa fa-wrench"></span> Gerer groupe</a></li>
                            <li><a href="{{route('groupe.create')}}"><span class="fa fa-file-text"></span> Creer groupe</a></li>                                              
                        </ul>                    
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Eglise</span></a>
                        <ul>
                            <li><a href="#"><span class="fa fa-image"></span> Gallerie</a></li>
                            <li><a href="#"><span class="fa fa-user"></span> Profile</a></li>
                            <li><a href="#"><span class="fa fa-users"></span> Caarnet Adresse</a></li>
                            <li class="xn-openable">
                                <a href="#"><span class="fa fa-envelope"></span> Broite email</a>
                                <ul>
                                    <li><a href="#"><span class="fa fa-inbox"></span> Inbox</a></li>
                                    <li><a href="#"><span class="fa fa-file-text"></span> Message</a></li>
                                    <li><a href="#"><span class="fa fa-pencil"></span> Compose</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><span class="fa fa-comments"></span> Messages</a></li>
                            <li><a href="#"><span class="fa fa-calendar"></span> Calendrier</a></li>
                            <li><a href="#"><span class="fa fa-edit"></span> Taches</a></li>
                        </ul>
                    </li>
                @endeglise
            </ul>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->
        
        <!-- PAGE CONTENT -->
        <div class="page-content">            
            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- TOGGLE NAVIGATION -->
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <!-- END TOGGLE NAVIGATION -->
                <!-- SEARCH -->
                <li class="xn-search">
                    <form role="form">
                        <input type="text" name="search" placeholder="Search..."/>
                    </form>
                </li>   
                <!-- END SEARCH -->
                <!-- SIGN OUT -->
                <li class="xn-icon-button pull-right">
                    <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                </li> 
                <!-- END SIGN OUT -->
                <!-- MESSAGES -->
                <li class="xn-icon-button pull-right">
                    <a href="#"><span class="fa fa-comments"></span></a>
                    <div class="informer informer-danger">4</div>
                    <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>                                
                            <div class="pull-right">
                                <span class="label label-danger">4 new</span>
                            </div>
                        </div>
                        <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-online"></div>
                                <img src="assets/images/users/user2.jpg" class="pull-left" alt="John Doe"/>
                                <span class="contacts-title">John Doe</span>
                                <p>Praesent placerat tellus id augue condimentum</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-away"></div>
                                <img src="assets/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>
                                <span class="contacts-title">Dmitry Ivaniuk</span>
                                <p>Donec risus sapien, sagittis et magna quis</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-away"></div>
                                <img src="assets/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>
                                <span class="contacts-title">Nadia Ali</span>
                                <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="list-group-status status-offline"></div>
                                <img src="assets/images/users/user6.jpg" class="pull-left" alt="Darth Vader"/>
                                <span class="contacts-title">Darth Vader</span>
                                <p>I want my money back!</p>
                            </a>
                        </div>     
                        <div class="panel-footer text-center">
                            <a href="pages-messages.html">Show all messages</a>
                        </div>                            
                    </div>                        
                </li>
                <!-- END MESSAGES -->
                <!-- TASKS -->
                <li class="xn-icon-button pull-right">
                    <a href="#"><span class="fa fa-tasks"></span></a>
                    <div class="informer informer-warning">3</div>
                    <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="fa fa-tasks"></span> Tasks</h3>                                
                            <div class="pull-right">
                                <span class="label label-warning">3 active</span>
                            </div>
                        </div>
                        <div class="panel-body list-group scroll" style="height: 200px;">                                
                            <a class="list-group-item" href="#">
                                <strong>Phasellus augue arcu, elementum</strong>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
                                </div>
                                <small class="text-muted">John Doe, 25 Sep 2014 / 50%</small>
                            </a>
                            <a class="list-group-item" href="#">
                                <strong>Aenean ac cursus</strong>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">80%</div>
                                </div>
                                <small class="text-muted">Dmitry Ivaniuk, 24 Sep 2014 / 80%</small>
                            </a>
                            <a class="list-group-item" href="#">
                                <strong>Lorem ipsum dolor</strong>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">95%</div>
                                </div>
                                <small class="text-muted">John Doe, 23 Sep 2014 / 95%</small>
                            </a>
                            <a class="list-group-item" href="#">
                                <strong>Cras suscipit ac quam at tincidunt.</strong>
                                <div class="progress progress-small">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                </div>
                                <small class="text-muted">John Doe, 21 Sep 2014 /</small><small class="text-success"> Done</small>
                            </a>                                
                        </div>     
                        <div class="panel-footer text-center">
                            <a href="pages-tasks.html">Show all tasks</a>
                        </div>                            
                    </div>                        
                </li>
                <!-- END TASKS -->
            </ul>
            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">
                @if(session('ok'))
                    <div class="alert alert-success msg" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {{ session('ok') }}
                    </div>
                @endif
                @yield('main')
            </div>
        </div>

    </div>
    <!-- END WIDGETS -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="pages-login.html" class="btn btn-success btn-lg" id="logout">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hide">
        {{ csrf_field() }}
    </form>
    <!-- END MESSAGE BOX-->    

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="{{ asset('audio/alert.mp3') }}" preload="auto"></audio>
    <audio id="audio-fail" src="{{  asset('audio/fail.mp3') }}" preload="auto"></audio>
    <!-- END PRELOADS -->                  
    
    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
<!--      -->
    <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}"></script>           
    <!-- END PLUGINS -->

    <script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script><!-- 
    <script type="text/javascript" src="{{ asset('js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> -->
    @yield('script')
    <script>
        $(() => {
            $('#logout').click((e) => {
                e.preventDefault()
                $('#logout-form').submit()
            })
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <!-- START THIS PAGE PLUGINS-->        
    <!-- END THIS PAGE PLUGINS-->        
    <!-- START TEMPLATE -->
    <script type="text/javascript" src="{{ asset('js/settings.js') }}"></script>    
    <script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>

</body>
</html>
