@extends('layouts.guest')

@section('container')
	<div class="login-container">
	    <div class="login-box animated fadeInDown">
	        <div class="login-logo"></div>
	        @yield('form')	        
	    </div>
	</div>    
@endsection