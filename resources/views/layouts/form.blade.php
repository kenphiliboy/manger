@extends('layouts.app')

@section('main')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.index') }}">Dashboard</a></li>
        <li><a href="{{ route('annonce.index') }}">Annonce</a></li>
        <li><a href="{{ route('annonce.create') }}">Nouvelle</a></li>
    </ul>
    <!-- START DEFAULT WIZARD -->
    <div class="block">
        <h4>Formulaire pour les differentes activites de la semaine</h4>
        <div class="wizard">
            <ul>
                <li>
                    <a href="#step-x">
                        <span class="stepNumber">0</span>
                        <span class="stepDesc">Annonce<br /><small>Pense de la semaine</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-1">
                        <span class="stepNumber">1</span>
                        <span class="stepDesc">Step 1<br /><small>Ecossa Reunion et Cult</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-2">
                        <span class="stepNumber">2</span>
                        <span class="stepDesc">Step 2<br /><small>Laic Diaconna Jeunesse</small></span>
                    </a>
                </li>
                <li>
                    <a href="#step-3">
                        <span class="stepNumber">3</span>
                        <span class="stepDesc">Step 3<br /><small>Musiques Divers Ancienna</small></span>                   
                    </a>
                </li>
                <li>
                    <a href="#step-4">
                        <span class="stepNumber">4</span>
                        <span class="stepDesc">Step 4<br /><small>Bon a savoir</small></span>                   
                    </a>
                </li>
            </ul>
            <div id="step-x">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4">
                        @yield('card-0')
                    </div>
                </div>
            </div>
            <div id="step-1">   
                <div class="row">
                    <div class="col-md-4">
                        @yield('card-1')
                    </div>
                    <div class="col-md-4">
                        @yield('card-2')
                    </div>
                    <div class="col-md-4">
                        @yield('card-3')
                    </div>          
                </div>
            </div>
            <div id="step-2">
                <div class="row">
                    <div class="col-md-4">
                        @yield('card-4')
                    </div>
                    <div class="col-md-4">
                        @yield('card-5')
                    </div>
                    <div class="col-md-4">
                        @yield('card-6')
                    </div>          
                </div>
            </div>                      
            <div id="step-3">
                <div class="row">
                    <div class="col-md-4">
                        @yield('card-7')
                    </div>
                    <div class="col-md-4">
                        @yield('card-8')
                    </div>
                    <div class="col-md-4">
                        @yield('card-9')
                    </div>          
                </div>
            </div>
            <div id="step-4">
                <h4>Step 4 Content</h4>
                <p>Les differentes annonces et convocation enregistrer ne sont pas automatiques disponible. Pour cela il faut passer de indisponible a disponible
                    dans la gestion des differentes annonces et convocations <a href="{{ route('annonce.index') }}"> ici...</a> </p>
            </div>                           

        </div>
    </div>                                       
    <!-- END DEFAULT WIZARD -->

    @section('script')        
        <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-datepicker.js') }} "></script>
        <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-timepicker.min.js') }} "></script>
        
        <script type="text/javascript" src="{{ asset('js/plugins/smartwizard/jquery.smartWizard-2.0.min.js') }}"></script>        
        <script type="text/javascript" src="{{ asset('js/plugins/jquery-validation/jquery.validate.js') }}"></script>

        <script type='text/javascript' src="{{ asset('js/plugins/noty/jquery.noty.js') }}"></script>
        <script type='text/javascript' src="{{ asset('js/plugins/noty/layouts/topRight.js') }}"></script>            
        <script type='text/javascript' src="{{ asset('js/plugins/noty/themes/default.js') }}"></script>
        <script>
        $(() => {
            $('.annonce').on('submit', function(e) {
                let that = $(e.currentTarget)
                e.preventDefault()
                console.log("url====> "+that.attr('action'));
                console.log("method====> "+that.attr('method'));
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: that.attr('method'),
                    url: that.attr('action'),
                    data: $(this).serialize(),
                    success: function(data) {
                        noty({text: data.msg, layout: 'topRight', type: 'success', timeout: 2500,});
                        console.log(JSON.stringify(data));
                    },
                    error: function (xhr,status,error) {            
                        //noty({text: JSON.stringify(error), layout: 'topRight', type: 'warning', timeout: 2500,});
                        console.log(JSON.stringify(xhr.responseText));
                    }
                });
                var time = $('datepicker').val();
                this.reset();
                $('input[name=pour_le]').val(time);
                $.noty.closeAll()
            });

            $('.classe_id, select[name=jour], select[name=club], select[name=departement]').change(function(){
                var data = @json(isset($data)?$data:'');
                var id = $(this).find(":selected").val();
                var model = '', token = '';
                var temp = $(this.form).find(':input');
                var table = {};
                var found = false;

               $(temp).each(function(){
                    var input = $(this);
                    if(input.attr('name') == 'model'){
                        model = input.val();
                    }else if(input.attr('name') == '_token'){
                        token = input.val();
                    }
                });
                
                model = model.capitalize();

                table = data.original[model];

                    console.log("Eloquent model: "+model);
                    
                    console.log("JSON string: "+JSON.stringify(table));
                
                switch(model){

                    case "Ecossa":    
                        field_form(temp, table, id, model, token);
                    break;

                    case "Laic":
                        field_form(temp, table, id, model, token);
                    break;

                    case "Cult":
                        field_form(temp, table, id, model, token);
                    break;

                    case "Reunion":
                        found = false;
                        $(table).each(function(index, value){
                            if(value.jour == id){
                                $(temp).each(function(){
                                    var input = $(this);
                                    if(input.attr('name') == 'model'){
                                        input.val(model)
                                    }else if(input.attr('name') == '_token'){
                                        input.val(token);
                                    }else if(input.attr('name') != 'status'){
                                        input.val(value[input.attr('name')]);
                                        found = true
                                    }
 
                                });
                            }
                        });
                        if(!found)
                            noty({text: "Non retrouve", layout: 'topRight', type: 'warning', timeout: 2500,});
                    break;

                    case "Jeunesse":
                        $(table).each(function(index, value){
                            if(value.club == id){
                                $(temp).each(function(){
                                    var input = $(this);
                                    if(input.attr('name') == 'model'){
                                        input.val(model)
                                    }else if(input.attr('name') == '_token'){
                                        input.val(token);
                                    }else if(input.attr('name') != 'status'){
                                        input.val(value[input.attr('name')]);
                                        found = true
                                    }
 
                                });
                            }
                        });
                        if(!found)
                            noty({text: "Non retrouve", layout: 'topRight', type: 'warning', timeout: 2500,});
                    break;

                }


            });
  
        })

        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }

        function field_form(field, tab, id, model, token)
        {
            $(tab).each(function(index, value){
                if(value.classe_id == id){
                    $(field).each(function(){
                        var input = $(this);
                        if(input.attr('name') == 'model'){
                            input.val(model)
                        }else if(input.attr('name') == '_token'){
                            input.val(token);
                        }else if(input.attr('name') != 'status'){
                            input.val(value[input.attr('name')]);
                        }
                        
                    });
                    return false;
                }

            });
        }
        </script>
    @endsection
@endsection
