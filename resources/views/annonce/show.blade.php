@extends('layouts.app')
  @section('content')
    <div class="container">
      <h4>La liste des eglises qui sont enregistres</h4>
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Adresse</th>
            <th>Responsable</th>
            <th>Statut</th>
            <th>Avatar</th>
            <th>Modifier</th>
            <th>Supprimer</th>
          </tr>
        </thead>
        <tbody>          
          @foreach($eglises as $eglise)
          <tr>
            <td>{{$eglise['id']}}</td>
            <td>{{$eglise['nom']}}</td>
            <td>{{$eglise['adresse']}}</td>
            <td>{{$eglise['responsable']}}</td>
            <td>{{$eglise['statut']}}</td>
            <td>{{$eglise['avatar']}}</td>            
            <td><a href="{{ route('eglise.edit', $eglise) }}" class="btn btn-warning">Modifier</a></td>
            <td><a href="{{ route('eglise.destroy', $eglise) }}" class="btn btn-danger">Supprimer</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  @endsection
 