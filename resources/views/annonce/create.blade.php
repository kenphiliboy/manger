@extends('layouts.form')


@section('card-0')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Penser de la semaine')
        @endslot

        @section('champs')
            {{ csrf_field() }}            
            @include('partials.form-area', [
                'title' => __('Penser de la semaine'),
                'type' => 'text',
                'name' => 'pense',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'annonce',
                ])
        @overwrite
    @endcomponent
@overwrite

@section('card-1')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Ecossa')
        @endslot

        @section('champs')
            {{ csrf_field() }}
            @include('partials.select-class',[
                'title'=> __('Eglise'),
                'name'=> 'classe_id',
                'options'=> $classes,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Directeur'),
                'type' => 'text',
                'name' => 'directeur',
                'place' => 'Directeur-(trice)',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Priere'),
                'type' => 'text',
                'name' => 'priere',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Rapport'),
                'type' => 'text',
                'name' => 'rapport',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('5minute'),
                'type' => 'text',
                'name' => 'meditation',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Bulletin'),
                'type' => 'text',
                'name' => 'bulletin',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'ecossa',
                ])
        @overwrite
    @endcomponent            
@overwrite

@section('card-4')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Laic')
        @endslot

        @section('champs')
            {{ csrf_field() }}

            @include('partials.select-class',[
                'title'=> __('Eglise'),
                'name'=> 'classe_id',
                'options'=> $classes,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Dierecteur'),
                'type' => 'text',
                'place' => 'Directeur-(trice)',
                'name' => 'directeur',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Rapport'),
                'type' => 'text',
                'name' => 'rapport',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'laic',
                ])
        @overwrite
    @endcomponent            
@overwrite

@section('card-3')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Cult')
        @endslot

        @section('champs')
            {{ csrf_field() }}

            @include('partials.select-class',[
                'title'=> __('Eglise'),
                'name'=> 'classe_id',
                'options'=> $classes,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Bienvenue'),
                'type' => 'text',
                'name' => 'bienvenue',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Appelle'),
                'type' => 'text',
                'name' => 'appelle',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Lecture'),
                'type' => 'text',
                'name' => 'lecture',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Priere'),
                'type' => 'text',
                'name' => 'priere',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Service'),
                'type' => 'text',
                'place' => 'Service de fidelite',
                'name' => 'service',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Sermont'),
                'type' => 'text',
                'name' => 'predication',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Remerciment'),
                'type' => 'text',
                'name' => 'remerciment',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'cult',
                ])
        @overwrite
    @endcomponent
@overwrite       

@section('card-2')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Siegant pour la semaine')
        @endslot

        @section('champs')
            {{ csrf_field() }}

            @include('partials.select-days', [
                'title' => __('Jour'),
                'name' => 'jour',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Bienvenue'),
                'type' => 'text',
                'name' => 'bienvenue',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Remerciment'),
                'type' => 'text',
                'name' => 'remerciment',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Predication'),
                'type' => 'text',
                'name' => 'meditation',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'reunion',
                ])
        @overwrite
    @endcomponent
@overwrite

@section('card-5')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Diaconna')
        @endslot

        @section('champs')
            {{ csrf_field() }}
            
            @include('partials.select-groupe', [
                'title' => __('Groupe'),
                'name' => 'groupe',
                'groupes' => $diaconna,
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'diacre',
                ])
        @overwrite
    @endcomponent
@overwrite

@section('card-6')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Jeunesse')
        @endslot

        @section('champs')
            {{ csrf_field() }}

            @include('partials.select-list', [
                'title' => __('Club'),
                'name' => 'club',
                'options' => ['Aventurier', 'Eclaireur', 'Segnor'],
                'required' => true,
                ])
            @include('partials.form-area', [
                'title' => __('Annonce'),
                'type' => 'text',
                'name' => 'info',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'jeunesse',
                ])            
        @overwrite
    @endcomponent
@overwrite

@section('card-7')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Musique')
        @endslot

        @section('champs')
            {{ csrf_field() }}

            @include('partials.select-groupe', [
                'title' => __('Groupe'),
                'name' => 'groupe',
                'groupes' => $musique,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Chant speciaux'),
                'type' => 'text',
                'name' => 'chant_special',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'musique',
                ])           
        @overwrite
    @endcomponent
@overwrite

@section('card-8')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Annonce divers')
        @endslot

        @section('champs')
            {{ csrf_field() }}

            @include('partials.select-list', [
                'title' => __('Departement'),
                'name' => 'departement',
                'options' => ["ancienna", "diacre", "ecossa", "laic", "cult", "jeunesse", "divers",
                                 "publication","famille","dorcas","education","fanm"],
                'required' => true,
                ])
            @include('partials.form-area', [
                'title' => __('Annonce'),
                'type' => 'text',
                'name' => 'info',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'divers',
                ])
        @overwrite
    @endcomponent
@overwrite

@section('card-9')

    @component('components.annonce', ['route' => route('annonce.store')])

        @slot('titre')
            @lang('Ancienna')
        @endslot

        @section('champs')
            {{ csrf_field() }}
            @include('partials.form-area', [
                'title' => __('Ancien'),
                'type' => 'text',
                'place' => 'Ancien de service pour la semaine',
                'name' => 'liste',
                'required' => true,
                ])
            @include('partials.hidden', [
                'value' => 'ancienna',
                ])
        @overwrite
    @endcomponent
@overwrite
