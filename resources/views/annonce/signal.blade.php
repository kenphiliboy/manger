@extends('layouts.form')

@section('card')

    @component('components.card')

        @slot('title')
            @lang('Signalé une église non existante')
        @endslot

        <form method="POST" action="">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
                <label for="category_id">@lang('Signalé cette église')</label>
                <select id="category_id" name="category_id" class="form-control">
                    @foreach($eglises as $eglise)
                        <option value="{{ $eglise->id }}">{{ $eglise->nom .' de '. $eglise->adresse }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="ok" name="ok" required>
                    <label class="custom-control-label" for="ok"> @lang('Je confirme que cette église n\'existe pas.')</label>
                </div>
            </div>
            @component('components.button', ['color'=>'warning'])
                @lang('Envoyer')

            @endcomponent
        </form>

    @endcomponent            

@endsection