@extends('layouts.app')
  @section('main')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.index') }}">Dashboard</a></li>
        <li><a href="{{ route('annonce.index') }}">Annonce</a></li>
        <li><a href="#" class="active">Afficher</a></li>
    </ul>
    <div class="row">
      <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
          <div class="panel-heading">                                
              <h3 class="panel-title">Liste des annonces</h3>
              <ul class="panel-controls">
                  <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                  <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                  <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
              </ul>                                
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped datatable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Statut</th>
                    <th>Penser</th>
                    <th>Consulter</th>
                    <th>Modifier</th>
                    <th>Supprimer</th>
                  </tr>
                </thead>
                <tbody>          
                  @foreach($annonces as $annonce)
                  <tr>
                    <td>{{ $annonce->id }}</td>
                    <td>{{ $annonce->pour_le }}</td>                                   
                    <td>
                      <form class="horizontal-form" action="{{ route('annonce.state', $annonce) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <input type="hidden" name="statut_update" value="{{ $annonce->statut }}">
                        <div class="form-group">
                          <button class="form-control btn btn-{{ $annonce->statut ? 'success' : 'warning' }}">{{ $annonce->statut ? 'disponible' : 'non-disponible' }}</button>
                        </div>                
                      </form>
                    </td>                  
                    <td>{{ $annonce->pense }}</td>                  
                    <td><a href="{{ route('annonce.show', $annonce) }}" class="btn btn-info form-control">Consulter</a></td>                  
                    <td>
                      <form class="horizontal-form" action="{{ route('annonce.edit', $annonce) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                          <button class="form-control btn btn-warning">@lang('Modifier')</button>
                        </div>                
                      </form>
                    </td>
                    <td>
                      <form class="horizontal-form" action="{{ route('annonce.destroy', $annonce->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div class="form-group">
                          <button class="form-control btn btn-danger">@lang('Supprimer')</button>
                        </div>                
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @section('script')
      <script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }} "></script>
      <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }} "></script>
      <script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script> 
    @endsection
  @endsection
 