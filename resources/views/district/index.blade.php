@extends('layouts.app')
  @section('main')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.index') }}">Dashboard</a></li>
        <li><a href="{{ route('district.index') }}">Distrie</a></li>
        <li><a href="{{ route('district.create') }}" class="active">Nouvelle</a></li>
    </ul>
    <div class="row">
      <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
          <div class="panel-heading">                                
              <h3 class="panel-title">Liste des distries enregistrer</h3>
              <ul class="panel-controls">
                  <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                  <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                  <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
              </ul>                                
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped datatable_simple">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Secteur</th>
                    <th>Eglise mere</th>
                    <th>Pasteur</th>
                    <th>Modifier</th>
                    <th>Supprimer</th>
                  </tr>
                </thead>
                <tbody>          
                  @foreach($distries as $distrie)
                  <tr>
                    <td>{{ $distrie->id }}</td>
                    <td>{{ $distrie->nom }}</td>
                    <td>secteur {{ $distrie->secteur }}</td>
                    <td>{{ $distrie->eglise_mere }}</td>
                    <td>{{ $distrie->responsable }}</td>
                    <td><a href="{{ route('district.edit', $distrie->id) }}" class="form-control btn btn-warning">Modifier</a></td>
                    <td>@include('partials.form-delete',['route'=> route('district.destroy', $distrie->id)])</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @section('script')
      <script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script> 
    @endsection
  @endsection
