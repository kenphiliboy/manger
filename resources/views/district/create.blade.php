@extends('layouts.form')

@section('main')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.index') }}">Dashboard</a></li>
        <li><a href="{{ route('district.index') }}">Distrie</a></li>
        <li><a href="{{ route('district.create') }}">Nouvelle</a></li>
    </ul>
    @component('components.register', [
        'route' => route('district.store')
        ])
        @slot('titre1')
            @lang('Ajouter une distrie')
        @endslot
        @slot('titre2')
            @lang('ici..')
        @endslot
        @section('champs')
            {{ csrf_field() }}
            @include('partials.form-group', [
                'title' => __('Nom'),
                'type' => 'text',
                'name' => 'nom',
                'place' => 'exemple: Bethesda de Petion ville',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Responsable'),
                'type' => 'text',
                'name' => 'responsable',
                'place' => 'Pasteur',                
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Eglise mere'),
                'type' => 'text',
                'name' => 'eglise_mere',
                'place' => 'Eglise mere',
                'required' => true,
                ])
            @include('partials.select-list', [
                'title' => __('Secteur'),
                'name' => 'secteur',
                'options' => ["1", "2", "3", "4", "5", "6", "7"],
                'required' => true,
                ])
            @include('partials.select-list', [
                'title' => __('Federation | Mission'),
                'name' => 'partie_de',
                'options' => ["Federation", "Mission"],
                'required' => true,
                ])
            <div class="form-group">
                <label class="col-md-4 col-xs-12 control-label"></label>
                <div class="col-md-6 col-xs-12">                                                                                                                                        
                    <label class="check"><input type="checkbox" class="icheckbox" checked="checked"/> @lang('Je confirme que cette église est mere de cette district.')</label>
                    <span class="help-block">confirmation obligatoire</span>
                </div>
            </div>
            @section('button')
                @component('components.button')
                    @lang('Envoyer')
                @endcomponent
                @component('components.button',[
                    'type' => 'reset',
                    'color' => 'default',
                    'pull' => 'left'])
                    @lang('Effacer')
                @endcomponent
            @endsection
        @endsection
    @endcomponent            
@endsection