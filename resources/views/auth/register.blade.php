@extends('layouts.form-log')
@section('form')
    @component('components.login')
        @slot('primary')
            @lang('Inscription')
        @endslot
        @slot('secondary')
            @lang('informatoin securiser')
        @endslot
        <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            @include('partials.form-log', [
                'place' => __('Nom'),
                'type' => 'text',
                'name' => 'nom_complet',
                'required' => true,
                ])
            @include('partials.form-log', [
                'place' => __('Telephone'),
                'type' => 'text',
                'name' => 'telephone',
                'required' => true,
                ])
            @include('partials.form-log', [
                'place' => __('Adresse email'),
                'type' => 'email',
                'name' => 'email',
                'required' => true,
                ])
            @include('partials.form-log', [
                'place' => __('Mot de passe'),
                'type' => 'password',
                'name' => 'password',
                'required' => true,
                ])
            @include('partials.form-log', [
                'place' => __('Confirmation du mot de passe'),
                'type' => 'password',
                'name' => 'password_confirmation',
                'required' => true,
                ])
            <div class="form-log">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="ok" name="ok" required>
                    <label class="custom-control-label" for="ok"> @lang('J\'accepte les termes et conditions de la politique de confidentialité.')</label>
                </div>
            </div>
            @component('components.button')
                @lang('Inscription')
            @endcomponent
            <a class="btn btn-link pull-left" href="{{ route('password.request') }}">
                @lang('Mot de passe oublié ?')
            </a>
        </form>
    @endcomponent
@endsection
