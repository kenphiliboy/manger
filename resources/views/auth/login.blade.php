@extends('layouts.form-log')
@section('form')
    @component('components.login')
        @slot('primary')
            @lang('Bienvenue')
        @endslot
        @slot('secondary')
            @lang('veuillez vous identifier')
        @endslot
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            @include('partials.form-log', [
                'place' => __('Adresse email ou profil'),
                'type' => 'email',
                'name' => 'email',
                'required' => true,
                ])
            @include('partials.form-log', [
                'place' => __('Mot de passe'),
                'type' => 'password',
                'name' => 'password',
                'required' => true,
                ])
            @include('partials.checkbox', [
                'title'=> 'se rappeler de moi',
                'required'=> true,
                ])
            @component('components.button', [
                'color'=> 'info'])
                @lang('Connexion')
            @endcomponent
            <a class="btn btn-link pull-left" href="{{ route('password.request') }}">
                @lang('Mot de passe oublié ?')
            </a>
        </form>
    @endcomponent
@endsection
