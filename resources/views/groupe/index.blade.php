@extends('layouts.app')
  @section('main')
  <ul class="breadcrumb">
      <li><a href="#">Home</a></li>                    
      <li><a href="#">Eglise</a></li>
      <li class="active">Liste eglise</li>
    </ul>
    <div class="row">
        <div class="col-md-12">
          <!-- START DEFAULT DATATABLE -->
          <div class="panel panel-default">
              <div class="panel-heading">                                
                <h3 class="panel-title">Liste des goupe enregistrer</h3>
                  <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                      <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                      <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                  </ul>                                
              </div>
              <div class="panel-body">
              <table class="table table-bordered table-striped datatable">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nom</th>
                  <th>departement</th>
                  <th>liste</th>
                  <th>Modifier</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>          
                @foreach($groupes as $groupe)
                <tr>
                  <td>{{ $groupe->id }}</td>
                  <td>{{ $groupe->nom }}</td>
                  <td>{{ $groupe->departement }}</td>
                  <td>{{ $groupe->liste }}</td>
                  <td><a href="{{ route('groupe.edit', $groupe->id) }}" class="btn btn-warning">Modifier</a></td>
                  <td> @include('partials.form-delete', ['route'=> route('groupe.destroy', $groupe->id)])</td>
                </tr>
                @endforeach          
              </tbody>
            </table>
          </div>
          <div class="panel-footer">
            <a href="{{ route('groupe.create') }}" class="btn btn-success">Creer gourpe</a>            
          </div>
      </div>
    </div>
  </div>
    @section('script')
      <script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script> 
    @endsection
  @endsection
 