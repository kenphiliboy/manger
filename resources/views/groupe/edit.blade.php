@extends('layouts.form')
    @section('main')
        <ul class="breadcrumb">
            <li><a href="{{ route('admin.index') }}">Home</a></li>
            <li><a href="{{ route('eglise.index') }}">Eglise</a></li>
            <li><a href="#">Nouveau groupe</a></li>
        </ul>
        @component('components.register', [
            'route' => route('groupe.update', $groupe) ])
            @slot('titre1')
                @lang('Ajouter un groupe')
            @endslot
            @slot('titre2')
                @lang('ici..')
            @endslot
            @section('champs')
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                @include('partials.select-list', [
                    'title' => __('Departement'),
                    'name' => 'departement',
                    'value' => $groupe->departement,
                    'options' => ["ancienna", "diaconna", "ecossa", "laic", "musique", "jeunesse",
                                     "publication","famille","dorcas","education","fanm"],
                    'required' => true,
                    ])
                @include('partials.form-group', [
                    'title' => __('Nom'),
                    'type' => 'text',
                    'name' => 'nom',
                    'value' => $groupe->nom,
                    'place' => 'nom du groupe',                
                    'required' => true,
                    ])
                @include('partials.form-area', [
                    'title' => __('Liste des membres'),
                    'type' => 'text',
                    'name' => 'liste',
                    'value' => $groupe->liste,
                    'required' => true,
                    ])
                @include('partials.hidden', [
                    'name' => 'variation',
                    'value'=> $groupe->id
                    ])
                @section('button')
                    @component('components.button')
                        @lang('Envoyer')
                    @endcomponent
                    @component('components.button',[
                        'type' => 'reset',
                        'color' => 'default',
                        'pull' => 'left'])
                        @lang('Effacer')
                    @endcomponent
                @endsection
            @endsection
        @endcomponent            
    @endsection