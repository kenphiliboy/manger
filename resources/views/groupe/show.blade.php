@extends('layouts.app')
  @section('content')
    <div class="container">
      <h4>La Distrie enregistrer est bien:</h4>
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Adresse</th>
            <th>Responsable</th>
            <th>Statut</th>
            <th>Avatar</th>
            <th>Modifier</th>
            <th>Supprimer</th>
          </tr>
        </thead>
        <tbody>          
          <tr>
            <td>{{$distrie->id}}</td>
            <td>{{$distrie->nom}}</td>
            <td>{{$distrie->adresse}}</td>
            <td>{{$distrie->responsable}}</td>
            <td>{{$distrie->statut}}</td>
            <td>{{$distrie->avatar}}</td>            
            <td><a href="{{ route('distrie.edit', $distrie) }}" class="btn btn-warning">Modifier</a></td>
            <td><a href="{{ route('distrie.destroy', $distrie) }}" class="btn btn-danger">Supprimer</td>
          </tr>
        </tbody>
      </table>
    </div>
  @endsection
 