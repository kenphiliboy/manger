@extends('layouts.form')

@section('main')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.index') }}">Home</a></li>
        <li><a href="#">Eglise</a></li>
        <li><a href="#">Nouvelle</a></li>
    </ul>
    @component('components.register', [
        'route' => route('eglise.store')])
        @slot('titre1')
            @lang('Ajouter une Eglise')
        @endslot
        @slot('titre2')
            @lang('ici..')
        @endslot
        @section('champs')
            {{ csrf_field() }}
            @include('partials.select-distrie', [
                'title'=> 'Distrie',
                'name'=> 'distrie_id',
                'distries'=> $distries,
                'required'=> true,
                ])
            @include('partials.form-group', [
                'title' => __('Nom'),
                'type' => 'text',
                'name' => 'nom',
                'place' => 'Betheseda de Petion ville',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Adresse'),
                'type' => 'text',
                'name' => 'adresse',
                'place' => 'Petion-ville bois-moquet rue borno #50',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Pseudo'),
                'place' => 'identifiant pour la connection connection',
                'type' => 'text',
                'name' => 'avatar',
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Mot de passe'),
                'place' => 'Mot de passe pour la connection',
                'type' => 'password',
                'name' => 'password',
                'required' => true,
                ])
            <div class="form-group">
                <label class="col-md-4 col-xs-12 control-label"></label>
                <div class="col-md-6 col-xs-12">          
                    <label class="check"><input type="checkbox" class="icheckbox" checked="checked"/> @lang('Je confirme que cette église est mere de cette distrie.')</label>
                    <span class="help-block">confirmation obligatoire</span>
                </div>
            </div>
            @section('button')
                @component('components.button')
                    @lang('Envoyer')
                @endcomponent
                @component('components.button',[
                    'type' => 'reset',
                    'color' => 'default',
                    'pull' => 'left'])
                    @lang('Effacer')
                @endcomponent
            @endsection
        @endsection
    @endcomponent            
@endsection