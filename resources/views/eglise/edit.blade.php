@extends('layouts.form')

@section('main')
    <ul class="breadcrumb">
        <li><a href="{{ route('admin.index') }}">Home</a></li>
        <li><a href="#">Eglise</a></li>
        <li><a href="#">mise a jour</a></li>
    </ul>
    @component('components.register', [
        'route' => route('eglise.update', $eglise)])
        @slot('titre1')
            @lang('Modifier une Eglise')
        @endslot
        @slot('titre2')
            @lang('ici..')
        @endslot
        @section('champs')
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            @include('partials.select-distrie',[
                'title'=> __('Distrie'),
                'name' => 'distrie_id',
                'value' => $eglise->distrie_id,
                'options' => $distries,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Nom'),
                'type' => 'text',
                'name' => 'nom',
                'value' => $eglise->nom,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Adresse'),
                'type' => 'text',
                'name' => 'adresse',
                'value' => $eglise->adresse,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Avatar'),
                'type' => 'text',
                'name' => 'avatar',
                'value' => $eglise->user_login,
                'required' => true,
                ])
            @include('partials.form-group', [
                'title' => __('Mot de passe'),
                'type' => 'password',
                'name' => 'password',
                'value' => $eglise->password,
                'required' => true,
                ])
            <div class="form-group">
                <label class="col-md-4 col-xs-12 control-label"></label>
                <div class="col-md-6 col-xs-12">
                    <label class="check"><input type="checkbox" name="ok" class="icheckbox"/> @lang('J\'ai change le mot de passe')</label>
                    <span class="help-block">si et seulment si vous avez change le mot de passe</span>
                </div>
            </div>
             
            @section('button')
                @component('components.button',[
                    'color' => 'warning'
                    ])
                    @lang('Envoyer')
                @endcomponent
                @component('components.button',[
                    'type' => 'reset',
                    'color' => 'default',
                    'pull' => 'left'])
                    @lang('Effacer')
                @endcomponent
            @endsection
        @endsection
    @endcomponent            
@endsection