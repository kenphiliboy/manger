@extends('layouts.app')
  @section('main')
  <ul class="breadcrumb">
      <li><a href="#">Home</a></li>                    
      <li><a href="#">Eglise</a></li>
      <li class="active">Liste eglise</li>
    </ul>
    <div class="row">
      	<div class="col-md-12">
	        <!-- START DEFAULT DATATABLE -->
	        <div class="panel panel-default">
	            <div class="panel-heading">                                
	            	<h3 class="panel-title">Liste des eglises enregistrer</h3>
	                <ul class="panel-controls">
	                	<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
	                  	<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
	                  	<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
	              	</ul>                                
	            </div>
	        <div class="panel-body">
	            <div class="table-responsive">
		            <table class="table table-bordered table-striped datatable_simple">
					        <thead>
					          <tr>
					            <th>ID</th>
					            <th>Nom</th>
					            <th>Adresse</th>
					            <th>Responsable</th>
					            <th>Distrie</th>
					            <th>Statut</th>
					            <th>Avatar</th>
					            <th>Modifier</th>
					            <th>Supprimer</th>
					          </tr>
					        </thead>
					        <tbody>          
					        	@foreach($eglises as $eglise)
					          	<tr>            
					            	<td>{{ $eglise->id }}</td>
						            <td>{{ $eglise->nom }}</td>
						            <td>{{ $eglise->adresse }}</td>
						            <td>{{ $eglise->distrie->responsable }}</td>
						            <th>{{ $eglise->distrie->nom }}</th>
						            <td>{{ $eglise->statut }}</td>
						            <td>{{ $eglise->user_login }}</td>            
						            <td><a href="{{ route('eglise.edit', $eglise) }}" class="btn btn-warning">Modifier</a></td>
						            <td> @include('partials.form-delete',['route'=>route('eglise.destroy', $eglise->id)])</td>
						        </tr>
					            @endforeach
					        </tbody>
				      	</table>	            	
		          </div>
				    </div>
				</div>
			</div>
		</div>
    @section('script')
      <script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script> 
    @endsection
  @endsection
 