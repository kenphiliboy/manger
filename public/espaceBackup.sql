-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: espace
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caracteristiques`
--

DROP TABLE IF EXISTS `caracteristiques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristiques` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `prix` double(8,2) NOT NULL,
  `size` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `couleur` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `produit_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `caracteristiques_produit_id_foreign` (`produit_id`),
  CONSTRAINT `caracteristiques_produit_id_foreign` FOREIGN KEY (`produit_id`) REFERENCES `produits` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristiques`
--

LOCK TABLES `caracteristiques` WRITE;
/*!40000 ALTER TABLE `caracteristiques` DISABLE KEYS */;
INSERT INTO `caracteristiques` VALUES (1,500.00,'10\'\'','bleu gris jeaune',1,'2019-03-18 21:26:26','2019-04-02 17:55:22'),(2,1500.00,'34','bleu',2,'2019-03-19 13:24:58','2019-03-19 13:24:58'),(3,200.00,'6,7,a,b','Rouge',3,'2019-03-19 19:30:42','2019-03-19 19:30:42'),(6,369.00,'1','Rouge',6,'2019-03-19 23:22:44','2019-03-19 23:22:44'),(9,25.00,'1','Clair',9,'2019-03-20 02:41:22','2019-03-20 02:41:22'),(13,1750.00,'1','Blanc',13,'2019-03-20 04:29:04','2019-04-07 03:14:46'),(14,2550.00,'1','Blanc',14,'2019-03-20 04:30:51','2019-04-07 03:13:31'),(15,100.00,'32','bleu, noir',15,'2019-03-20 15:51:44','2019-03-20 15:51:44'),(16,650.00,'15\'','Blanc',16,'2019-03-20 17:58:16','2019-03-20 17:58:16'),(19,1100.00,'13','Gris',19,'2019-03-21 02:55:50','2019-03-21 02:55:50'),(21,10500.00,'13 * 8','gris',21,'2019-03-21 18:25:48','2019-03-21 18:25:48'),(22,44000.00,'null','Gris',22,'2019-03-28 21:53:23','2019-03-28 21:53:23'),(23,3000.00,'M','noir & blan',23,'2019-03-28 22:03:37','2019-03-31 22:53:54'),(24,50.00,'1','Clair',24,'2019-03-30 01:16:10','2019-03-30 01:16:10'),(25,5800.00,'1','Noir et blanc',25,'2019-03-31 22:17:27','2019-04-07 03:14:14'),(26,15.00,'5','Noir et blanc',26,'2019-03-31 22:23:29','2019-03-31 22:23:29'),(27,350.00,'5','Blanche',27,'2019-04-01 01:35:11','2019-04-01 01:35:11'),(28,2500.00,'34','Blan noir gris',28,'2019-05-28 19:35:29','2019-05-28 19:35:29'),(29,2500.00,'36','Gris',29,'2019-05-28 19:37:57','2019-05-28 19:37:57');
/*!40000 ALTER TABLE `caracteristiques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `categorie` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_categorie_unique` (`categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Vêtements','2019-03-18 18:34:02','2019-04-12 21:08:45'),(2,'Accessoires','2019-03-19 00:25:49','2019-03-19 00:25:49'),(3,'Peinture','2019-03-31 09:09:02','2019-03-31 09:09:02'),(4,'Sculpture','2019-04-09 03:50:32','2019-04-09 03:50:32'),(5,'produit comestible','2019-04-14 23:56:39','2019-04-14 23:56:39'),(6,'soins des cheveux','2019-05-07 00:33:33','2019-05-07 00:33:33'),(7,'tous pour les cheveux','2019-05-07 00:39:53','2019-05-07 00:39:53');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `caracteristique_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `images_user_id_foreign` (`user_id`),
  KEY `images_caracteristique_id_foreign` (`caracteristique_id`),
  CONSTRAINT `images_caracteristique_id_foreign` FOREIGN KEY (`caracteristique_id`) REFERENCES `caracteristiques` (`id`) ON DELETE CASCADE,
  CONSTRAINT `images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'fCoNZQugxfoq64I9Y4n1rMmibVmnz2iGgoTb4j8m.jpeg',1,1,'2019-03-18 21:26:26','2019-04-02 17:56:37'),(2,'40wzhYqiiVtUITUQwEQpygYg2VbPbEV5VQvSyHPD.jpeg',1,1,'2019-03-18 21:26:26','2019-04-02 17:56:52'),(3,'W57RvolLOQNAFr2cHYr5BZGtGOf9GJIUw0ygVpGy.jpeg',1,2,'2019-03-19 13:24:59','2019-04-04 18:07:50'),(4,'6P4Qv2HruyEu5MjoPJVfLz8UqI1sf2xibUKGVEVX.jpeg',1,2,'2019-03-19 13:24:59','2019-04-04 18:10:08'),(5,'nfjEMNhFU1VS9soypdd3Q5oQFr472EsKckzUJzVl.jpeg',10,3,'2019-03-19 19:30:42','2019-03-19 19:30:42'),(6,'cds2ibagC7B2nvS2FiD52GQiFYMopXWEUbyfkrlp.jpeg',10,3,'2019-03-19 19:30:42','2019-03-19 19:30:42'),(9,'Y5G3YdFgfy88y6BmLE2xN0qQEWv6ehBDa58In0dR.jpeg',8,6,'2019-03-19 23:22:44','2019-04-04 21:50:34'),(10,'vSFtPcRqbaGasBemNQN0Er4JeR53zRbBgKxyx1OQ.jpeg',8,6,'2019-03-19 23:22:44','2019-04-04 21:18:36'),(11,'KlIHLRT5jw5tzPe13p3w1Z5G8Zj4Y0Xpah8FpOaV.jpeg',8,9,'2019-03-20 02:41:22','2019-04-05 17:21:43'),(12,'3mVZekEp1bWQKEi3jzvWV3GWoyBeUh065PZQj095.jpeg',8,9,'2019-03-20 02:41:23','2019-03-20 02:41:23'),(16,'WPL27QB6AsbY8EQFnPRz7hIWhiZ7SE1AUpGsMYFJ.jpeg',14,13,'2019-03-20 04:29:04','2019-03-20 04:29:04'),(17,'7Nx77QCj4irtb7xGeHXNp6TYksEVLHNNUOp9Ei66.png',14,13,'2019-03-20 04:29:04','2019-03-20 04:29:04'),(18,'pAS6IaSFtiQ9wtI6QUJkHRmRol4xFZD1Srlr8LnN.jpeg',14,14,'2019-03-20 04:30:52','2019-03-20 04:30:52'),(19,'1MIGUUdB4XL73aFQ3PsmXWuaf1tgFpfcwqvFM1eD.png',14,14,'2019-03-20 04:30:52','2019-03-20 04:30:52'),(20,'XwogiuttAvDShG1lyVJ781iIXAe8XmzbcAuRz4cK.jpeg',19,16,'2019-03-20 17:58:16','2019-03-20 17:58:16'),(21,'HhuxSHGrdqsh82qCcCyK88NFksTGF5Wxby0lFg58.jpeg',19,16,'2019-03-20 17:58:16','2019-03-20 17:58:16'),(24,'lmxBVoa2RV3xRcBz2CKaXUo9HqpD7E5HQ22rzTJ3.png',21,19,'2019-03-21 02:55:51','2019-03-21 02:55:51'),(25,'NWD6wYKz8FlcHq6zzxXn1hKMSYNQpbIIT5eKhmfn.png',21,19,'2019-03-21 02:55:51','2019-03-21 02:55:51'),(27,'yogkoJQqMUWQ27iSz54DPHsLZoftZnTgSVq5oZ6i.jpeg',23,21,'2019-03-21 18:25:48','2019-03-21 18:25:48'),(28,'XBD2j7QiEZ1E1bYgHOQTQa5CwJFBHadSd0HRz5E3.jpeg',23,21,'2019-03-21 18:25:48','2019-03-21 18:25:48'),(29,'ntvU4HR6jF7E2ykVAYOYEyWh6FX7YKklbivPaA3E.png',21,22,'2019-03-28 21:53:23','2019-03-28 21:53:23'),(30,'9NnUOd2xerJXQUm5hw6lvfy88mnQ56yzAKrYH9FQ.png',21,22,'2019-03-28 21:53:23','2019-03-28 21:53:23'),(31,'ORYZZFSIvjc2eTwyrZmN9j06uSIgdvlTTNuMhdCt.jpeg',1,23,'2019-03-28 22:03:37','2019-03-28 22:03:37'),(32,'iSuyU5jWx9HJc7l25g2ngkPvXB6z58BgVAzgy44q.jpeg',1,23,'2019-03-28 22:03:37','2019-03-28 22:03:37'),(33,'s7uJU1NJeysj9kqjVCOgaeX95NZ2n38R1o9DLBbt.jpeg',8,24,'2019-03-30 01:16:10','2019-03-30 01:16:10'),(34,'YnUSNmGoy7IHic0a41MDivoPWSWAeD3k0qJAX4Hs.jpeg',8,24,'2019-03-30 01:16:10','2019-03-30 01:16:10'),(35,'hl5rDe404tVTrQ4yvDZ7Z0f2roj8RuHgHjEsk1Os.jpeg',14,25,'2019-03-31 22:17:27','2019-03-31 22:17:27'),(36,'KkFsEWZJMqelY2nmZNEyEHGUkkAAz86cZECspSwg.jpeg',14,25,'2019-03-31 22:17:27','2019-03-31 22:17:27'),(37,'vjrKlLzKvO4NVpjQiRQPO04O6CbU6TT0Svwn6n9u.jpeg',14,26,'2019-03-31 22:23:29','2019-03-31 22:23:29'),(38,'wDe9IHatk9QphtWjxyCgIuQuufnckBVTO35BQAwT.jpeg',14,27,'2019-04-01 01:35:11','2019-04-01 01:35:11'),(39,'7Q3x9WIas5zx3FOLU0KPTzfqi7BVGdus3s67QwVX.jpeg',1,15,'2019-04-02 18:11:10','2019-04-02 18:11:10'),(40,'cbLutoCIfD0aQAsMvc3yVGdjCkb5nulgQ4NjMkjo.jpeg',1,15,'2019-04-02 18:11:10','2019-04-02 18:11:10'),(41,'zuvPE2LLzRBjdEyMftAIYdO5CSBXRcEovt6McAzk.jpeg',50,28,'2019-05-28 19:35:29','2019-05-28 19:35:29'),(42,'uPiEBolb5eNYjuH64HHpw5OJUqo9ewjaUogMMjWs.jpeg',50,28,'2019-05-28 19:35:29','2019-05-28 19:35:29'),(43,'ORP348lIhnO9MY3YsSwxrwGcf74eGS9GernrRcN4.jpeg',50,29,'2019-05-28 19:37:57','2019-05-28 19:37:57'),(44,'mK4m2KPssK7safzcvjrxuVOZl5qixLfXefs7GX0N.jpeg',50,29,'2019-05-28 19:37:57','2019-05-28 19:37:57');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `markets`
--

DROP TABLE IF EXISTS `markets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `markets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `nom` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categorie_id` bigint(20) unsigned NOT NULL,
  `statut` tinyint(1) NOT NULL DEFAULT '0',
  `secteur` enum('formel','informel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'informel',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `markets_nom_unique` (`nom`),
  UNIQUE KEY `markets_telephone_unique` (`telephone`),
  KEY `markets_categorie_id_foreign` (`categorie_id`),
  KEY `markets_user_id_foreign` (`user_id`),
  CONSTRAINT `markets_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `markets`
--

LOCK TABLES `markets` WRITE;
/*!40000 ALTER TABLE `markets` DISABLE KEYS */;
INSERT INTO `markets` VALUES (1,1,'PhiliProdArt','Petion ville Montagne noire impace Georges','(509) 47-51-0810',1,0,'informel','2019-03-18 21:24:04','2019-03-18 21:24:04'),(2,4,'Villa\'s','Croix-des-Bouquets lilavois 62','50931920065',1,0,'informel','2019-03-19 00:07:15','2019-03-19 00:07:15'),(3,10,'Okstore','Tabarre 52','47811428',2,0,'informel','2019-03-19 19:27:11','2019-03-19 19:27:11'),(4,11,'Doris','Delmas 33 rue défilé #40','44338709',1,0,'informel','2019-03-19 22:47:57','2019-03-19 22:47:57'),(5,8,'Studio Beo','Petion-ville , rue matin #23','50945091668',2,0,'informel','2019-03-19 23:19:19','2019-03-19 23:19:19'),(6,13,'Wilalex','Delmas, rue Dorsaintvil','6778886',2,0,'informel','2019-03-19 23:53:07','2019-03-19 23:53:07'),(7,6,'Lucien','PV','38154446',2,0,'informel','2019-03-20 02:22:26','2019-03-20 02:22:26'),(8,14,'HTEC','82, Rue Borno Petion-ville','(509)43-04-6328',2,0,'informel','2019-03-20 04:10:19','2019-03-20 04:10:19'),(9,15,'Ma Boutique','8,impasse Jovial','50936290666o',1,0,'informel','2019-03-20 15:48:30','2019-03-20 15:48:30'),(10,16,'MyStuff','Carrefour','50933516565',2,0,'informel','2019-03-20 16:33:49','2019-03-20 16:33:49'),(11,19,'Nicky\'s Electronic shop','29, delmas 19 Port-au-prince Haiti','4471-0175',2,0,'informel','2019-03-20 17:42:30','2019-03-20 17:42:30'),(12,21,'Teph Gab','Petion vile rue mettelus #40','34083550',2,0,'informel','2019-03-21 02:16:48','2019-03-21 02:16:48'),(13,22,'Mondyfashion','Carrefour bizoton','40473008',1,0,'informel','2019-03-21 14:52:52','2019-03-21 14:52:52'),(14,23,'Bruce boutique','Petion vile rue mettelus #44','50944077358',1,0,'informel','2019-03-21 18:20:26','2019-03-21 18:20:26'),(15,24,'Pati\'s creation','Delmas 60','(509) 44-57-2666',2,0,'informel','2019-03-21 22:36:28','2019-03-21 22:36:28'),(16,26,'bla boa','carreefour','566828282',2,0,'informel','2019-03-23 05:47:37','2019-03-23 05:47:37'),(17,27,'Phyl','Petion Ville Nerette','44255851',2,0,'informel','2019-03-27 15:37:04','2019-03-27 15:37:04'),(18,29,'Slowpen','Bois moquette','31282407',2,0,'informel','2019-03-31 00:37:54','2019-03-31 00:37:54'),(19,30,'Agella Dasten','Delmas 38#12','(509)37165245',2,0,'informel','2019-04-02 17:44:32','2019-04-02 17:44:32'),(20,33,'blaisio','delmas 19 rue cineraire','(509)38-02-4976',2,0,'informel','2019-04-04 00:58:32','2019-04-04 00:58:32'),(21,35,'Ms prodz','Delmas','50932370194',2,0,'informel','2019-04-07 19:08:35','2019-04-07 19:08:35'),(22,36,'Pistamama','44,impasse Charles oscar, route de Frères','49102513',2,0,'informel','2019-04-08 03:15:44','2019-04-08 03:15:44'),(23,38,'Pop marchandise','Delmas 33','31857940',2,0,'informel','2019-04-08 16:16:04','2019-04-08 16:16:04'),(24,2,'Techno','delmas 75','(509) 44712603',2,0,'informel','2019-04-12 20:57:57','2019-04-12 20:57:57'),(25,44,'Cla-an Store','Delmas','50941784523',1,0,'informel','2019-04-12 20:59:28','2019-04-12 20:59:28'),(26,45,'entreprise  candy','rue gregroire prolongee petion-ville haiti tete de l\'eau','3463-80-48',5,0,'informel','2019-04-15 00:05:24','2019-04-15 00:05:24'),(27,46,'Yvenold LUCIEN','#6c, Bas-Duval,Rue Fleur de Corail, Rte de Frères 28, P-V','509 36292261',4,0,'informel','2019-04-18 03:08:16','2019-04-18 03:08:16'),(28,46,'Yvenold LUCIEN ...','#6c, Bas-Duval,Rue Fleur de Corail, Rte de Frères 28, P-V','509 37469720',4,0,'informel','2019-04-18 03:11:17','2019-04-18 03:11:17'),(29,48,'chic beauté boutik','Delmas 31,rue Vernet#8','47316517',2,0,'informel','2019-05-07 00:25:36','2019-05-07 00:25:36'),(30,49,'chic beauté ma boutik','Delmas 31 rue Vernet','50947316517',6,0,'informel','2019-05-07 00:52:12','2019-05-07 00:52:12'),(31,50,'Bòz\'or','Delmas 33 imp G.women','4012361',2,0,'informel','2019-05-28 19:31:40','2019-05-28 19:31:40');
/*!40000 ALTER TABLE `markets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_03_11_193000_create_permission_tables',1),(4,'2019_03_11_194221_categorie_table',1),(5,'2019_03_11_202408_create_market_table',1),(6,'2019_03_12_001014_create_produit_table',1),(7,'2019_03_12_014131_create_caracteristique_table',1),(8,'2019_03_12_020058_create_images_table',1),(9,'2019_03_12_061328_create_stocks_table',1),(10,'2019_03_18_230803_create_posts_table',2),(11,'2019_03_19_164928_update_user_table',3),(12,'2016_06_01_000001_create_oauth_auth_codes_table',4),(13,'2016_06_01_000002_create_oauth_access_tokens_table',4),(14,'2016_06_01_000003_create_oauth_refresh_tokens_table',4),(15,'2016_06_01_000004_create_oauth_clients_table',4),(16,'2016_06_01_000005_create_oauth_personal_access_clients_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('aaltidort@yahoo.com','$2y$10$vuZFT6jrroOmIKdQl/JJJuWBny0ogtzBLlp1n55Wz5oJ8A3KYvVgm','2019-03-20 11:26:00'),('pdjelso@gmail.com','$2y$10$pYb/F7O2p0ViScKfZmg0huR.wCAIBBv89yNWzNyBZuNLdLOj.1CuO','2019-03-20 15:58:37'),('stephgabo@gmail.com','$2y$10$Ud4zcCUt9N0D6yNxZxH.f.VwtF8vqgoK5ebbxyVAbjxlIag.EUdI2','2019-03-22 02:26:56'),('ashleyphili202@gmail.com','$2y$10$xF7pl2dLgIdMks3HRLe.9OWVpYLNpT9hQO4xnOt/aWfdEG0FODi5.','2019-03-22 02:38:32'),('kenphiliboy@gmail.com','$2y$10$ArSpRo7lR.5bYKwl.cDDIOQvmG.07W0tRA9uHHSrSNMruaw2gRbdu','2019-03-22 05:53:56'),('bellunetabithamegane@gmail.com','$2y$10$LpSgE5OnZsiN7bZrUpgJKu324j.LXpwY/9VtHwiOd/AEk4eKADKbK','2019-03-29 22:31:09'),('rokennyj3623@gmail.com','$2y$10$onTmDldaERcl9zvyO4F3LueOt3RwfFuhbwg0qdxLUHldk.HQkosBy','2019-04-03 14:27:09'),('bonbgy23@gmail.com','$2y$10$i8JB8XsVDkcQm9/5e6qVe.9ZBQ2I3kSjmf9Vzf1MhtBELKd2XX.LK','2019-04-04 22:21:54'),('ambroisebertholin455@gmail.com','$2y$10$BGxjgZNQKlftR/JTM6LqNO8HCQ4z22EH5/WyIJRMDRmbRgT4qOvwK','2019-04-08 15:47:43'),('Yve-LArt@yahoo.com','$2y$10$z/3ycsFCQZmQhUoIincNTuIpq9XoqpL.VT7i0g.uldwBQpp8LcB8K','2019-04-10 01:05:57'),('Yve-L.Art@yahoo.com','$2y$10$OZPOAmT3vnXwIoH.g.fjNe2JIQDUc758TXhWLCK8qaE.a.tOLO3ya','2019-04-26 19:02:33'),('www.ginette.mvl@gmail.com','$2y$10$32oF7E4XmJN1JSdlVAFih.MwWjPTljoFTWHc572S9x/uLpuo/X5na','2019-05-07 00:43:35'),('ginette.mvl@gmail.com','$2y$10$g0NvLxU7.jvsgoM0L9uQeuK8638oQHZl31O3EY3RZyUfIhQ4OUhQW','2019-05-07 02:30:43');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `commentaire` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_id_foreign` (`user_id`),
  CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,'Ben deja mwen di nou mesi paske nou pran tan nou poun pase gade e deja mpral komanse panse pou tout remak nap fem yo deja mwen di nou mesi anko.','2019-03-19 00:01:16','2019-03-19 00:01:16'),(2,8,'Genyen anpil lien ki pou ranje nan menu an pase gen ki voyem nan 404.','2019-03-19 03:22:07','2019-03-19 03:22:07'),(3,8,'Genyen anpil lien ki pou ranje nan menu an pase gen ki voyem nan 404.','2019-03-19 03:22:07','2019-03-19 03:22:07'),(4,8,'Genyen anpil lien ki pou ranje nan menu an pase gen ki voyem nan 404.','2019-03-19 03:22:07','2019-03-19 03:22:07'),(5,8,'Pati komantè an sanble gen bug pase komantè yo ale 2 fois.  Fok nou ta ranje erè òtographe yo. Teste li sou plizyè platfòm ak browser.  DataTables yo sanble pa responsiv.','2019-03-19 03:25:05','2019-03-19 03:25:05'),(6,1,'ok map pran yo an konsideration si yon komantaire ale plis ke yon fwa se ou ki voye\'l plizye fwa kom ou sou yon android tou sa depann de sansibilite touch la tou','2019-03-19 03:41:27','2019-03-19 03:41:27'),(7,11,'Good job brother keep going like that.','2019-03-19 22:51:03','2019-03-19 22:51:03'),(8,8,'Dashbord lan sanble pako finn modifye men lyen an kòrek. Mw paka ajoute yon produit li voyem nan yon paj erreur (500). Mw paka liste produit.','2019-03-19 23:57:07','2019-03-19 23:57:07'),(9,13,'Bon travay. Lè wap ajoute yon produit li pa vle','2019-03-20 00:18:38','2019-03-20 00:18:38'),(10,1,'pou ajoute produit a fok nou sur ke nou upload image yo si non li pap pase  pou admin lan li pat mache a Tabie te gen produit ki upload san image map gere\'l on jan poul toujou mache moun nan a mete imaj lan apres','2019-03-20 01:43:00','2019-03-20 01:43:00'),(11,6,'Byen jere Dashboard la','2019-03-20 02:24:59','2019-03-20 02:24:59'),(12,6,'Byen jere Dashboard la','2019-03-20 02:25:02','2019-03-20 02:25:02'),(13,6,'Texte yo tou','2019-03-20 02:25:37','2019-03-20 02:25:37'),(14,6,'Bon travay broh!','2019-03-20 02:26:19','2019-03-20 02:26:19'),(15,6,'Komantè a ale sanm pa voye\'l ale','2019-03-20 02:43:53','2019-03-20 02:43:53'),(16,1,'ok bro merci','2019-03-20 14:58:00','2019-03-20 14:58:00'),(17,16,'Lem eseye post yon produit, li dim \"The statut field is required.\" m pa vraiman we kisa m pa ranpli. Men koman w konte jere sa? eske se a partir de moncash? ou deja planifye koman wap asire ke le yon moun peye lap jwen produit a?','2019-03-20 16:57:48','2019-03-20 16:57:48'),(18,16,'Apre sa m panse ke ide a bn, depi w rive regle tout detay teknik yo, se design lan wap ret pou w travay','2019-03-20 17:00:00','2019-03-20 17:00:00'),(19,19,'bon, travay la bel mw renmen ide a men gen anpil travay toujou ki pou fet. Modification produit yo pa mache, dashboard la ta dwe koresponn a boutique pam nan mwen panse paske mw we dashboard sa bay yo vue global sou tt system nan. eske c selman admin lap ye? apre son bon travay','2019-03-20 18:05:58','2019-03-20 18:05:58'),(20,21,'Kek ti problem pou fin jere par exemple lew ap creer produit ... Men bon bagay frèm!','2019-03-21 03:50:23','2019-03-21 03:50:23'),(21,3,'Bon travay sof li manke plus design kek bel ti slide kap bay anvi vizitel nan page d\'accueil man. Après je monte à bord bro','2019-04-04 22:04:04','2019-04-04 22:04:04'),(22,1,'ok map fe sa oui zanmi pam men c an dernier mkite dernier touch sa yo poum bay pi module sa yo ki la se pa pou simple particulier kap vin hte men c pou moun kap fe livraison moun kap vann donk mte slide ak bel ti fleure pou pati client ki sou lot lien an bro deja merci bro...','2019-04-05 17:14:53','2019-04-05 17:14:53'),(23,45,'mwen panse ke nan phase essai  application an personnellement  mwen  satisafe et map tann nvlle mise a jour yo','2019-04-15 00:14:55','2019-04-15 00:14:55');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produits`
--

DROP TABLE IF EXISTS `produits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rabais` int(11) NOT NULL DEFAULT '0',
  `statut` tinyint(1) NOT NULL DEFAULT '0',
  `demande` int(10) unsigned NOT NULL DEFAULT '0',
  `market_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produits_market_id_foreign` (`market_id`),
  CONSTRAINT `produits_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produits`
--

LOCK TABLES `produits` WRITE;
/*!40000 ALTER TABLE `produits` DISABLE KEYS */;
INSERT INTO `produits` VALUES (1,'Blasios','Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute',2,1,0,1,'2019-03-18 21:26:26','2019-04-02 17:55:22'),(2,'pantalon','skini tout Jan tout models',4,1,0,1,'2019-03-19 13:24:58','2019-03-19 13:24:58'),(3,'Htg','Fckii',0,1,0,3,'2019-03-19 19:30:42','2019-03-19 19:30:42'),(6,'Permanante','Permanante',0,1,0,5,'2019-03-19 23:22:44','2019-03-19 23:22:44'),(9,'Nails','Nails',0,1,0,5,'2019-03-20 02:41:22','2019-03-20 02:41:22'),(13,'LBE-5AC-Gen2 Ubiquiti LiteBeam AC 5 GHz Unlicensed Radio Bridge','Network interconnection device',5,1,0,8,'2019-03-20 04:29:04','2019-03-20 04:29:04'),(14,'AirMax Ubiquiti','Network interconnection device',5,1,0,8,'2019-03-20 04:30:51','2019-03-20 04:30:51'),(15,'Jeans','jeans',2,1,0,9,'2019-03-20 15:51:44','2019-03-20 15:51:44'),(16,'Dell All in one 1TB','Ordinateur de bureau tout en un',5,1,0,11,'2019-03-20 17:58:16','2019-03-20 17:58:16'),(19,'Galaxy S0+','Galaxy S10+ etat neuf',5,1,0,12,'2019-03-21 02:55:50','2019-03-21 02:55:50'),(21,'Htc','htc etat neuf',10,1,0,14,'2019-03-21 18:25:48','2019-03-21 18:25:48'),(22,'Rav4 2010','Rav4 2010',10,1,0,12,'2019-03-28 21:53:23','2019-03-28 22:05:53'),(23,'ken & queen','pour les amoureux uniquement',10,1,0,1,'2019-03-28 22:03:37','2019-03-28 22:03:37'),(24,'Aceton','Aceton pour les ongles',1,1,0,5,'2019-03-30 01:16:10','2019-03-30 01:16:10'),(25,'Alarme de maison','Security devices',0,1,0,8,'2019-03-31 22:17:27','2019-03-31 22:17:27'),(26,'Camera Surveillance','Security devices',0,1,0,8,'2019-03-31 22:23:29','2019-03-31 22:23:29'),(27,'Camera Surveillances','Security Cam devices',0,1,0,8,'2019-04-01 01:35:11','2019-04-01 01:35:11'),(28,'Skini','Pour homme uniquement pour ceux qui ont la classe',2,1,0,31,'2019-05-28 19:35:29','2019-05-28 19:35:29'),(29,'Pantalon','framboise sur le côté Gris',0,1,0,31,'2019-05-28 19:37:57','2019-05-28 19:37:57');
/*!40000 ALTER TABLE `produits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quantite` int(10) unsigned NOT NULL,
  `epuise` int(10) unsigned NOT NULL DEFAULT '0',
  `disponible` tinyint(1) NOT NULL DEFAULT '1',
  `caracteristique_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stocks_caracteristique_id_foreign` (`caracteristique_id`),
  CONSTRAINT `stocks_caracteristique_id_foreign` FOREIGN KEY (`caracteristique_id`) REFERENCES `caracteristiques` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks`
--

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
INSERT INTO `stocks` VALUES (1,9,0,1,1,'2019-03-18 21:26:26','2019-04-02 17:55:22'),(2,8,0,1,2,'2019-03-19 13:24:59','2019-03-19 13:24:59'),(3,25,0,1,3,'2019-03-19 19:30:42','2019-03-19 19:30:42'),(5,3,0,1,6,'2019-03-19 23:22:44','2019-03-19 23:22:44'),(6,699,0,1,9,'2019-03-20 02:41:23','2019-03-20 02:41:23'),(7,20,0,1,13,'2019-03-20 04:29:04','2019-03-20 04:29:04'),(8,20,0,1,14,'2019-03-20 04:30:52','2019-03-20 04:30:52'),(9,6,0,1,16,'2019-03-20 17:58:16','2019-03-20 17:58:16'),(10,10,0,1,19,'2019-03-21 02:55:51','2019-03-21 02:55:51'),(11,30,0,1,21,'2019-03-21 18:25:48','2019-03-21 18:25:48'),(12,1,0,1,22,'2019-03-28 21:53:23','2019-03-28 21:53:23'),(13,7,0,1,23,'2019-03-28 22:03:37','2019-03-28 22:03:37'),(14,36,0,1,24,'2019-03-30 01:16:10','2019-03-30 01:16:10'),(15,20,0,1,25,'2019-03-31 22:17:27','2019-03-31 22:17:27'),(16,17,0,1,15,'2019-04-02 18:11:10','2019-04-02 18:11:10'),(17,14,0,1,28,'2019-05-28 19:35:29','2019-05-28 19:35:29'),(18,17,0,1,29,'2019-05-28 19:37:57','2019-05-28 19:37:57');
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cin_nif` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'kendy','kenphiliboy@gmail.com',NULL,'$2y$10$jjHVRU3CX618Flnvou1GUuQ0NgospVtOj0gBBDwRnPgVRf1W4p7gm','(509) 47-51-0810','01-01-1992-09-00875','Petion ville Montagne noire impace Georges','qoNfQirH7LPycUVW93HLCTckJg2PT8nFgD7ZZr7fk3HyEQBXMZjc8HnthbCA','2019-03-18 18:30:39','2019-03-19 21:52:22'),(2,'Bruny Gby','bruny@gmail.com',NULL,'$2y$10$nvz8jYAdlfOqGkEiVL8vg.Smqd8up0ElP5PPwA1iqbdxTCBX5WWV.',NULL,NULL,NULL,'Gp54wSJc9BSs5CYLMRaYHquYjzDA0etSoSE5JxXJ5aEEgCDTAQ86R6FEmD0x','2019-03-18 19:58:12','2019-03-18 19:58:12'),(3,'Jack','jacquesalande@gmail.com',NULL,'$2y$10$IvBTteP0tjRmBVIxmc7xF.HspyEVW/7Ndvd4nlKYO59uzSr09asTS',NULL,NULL,NULL,NULL,'2019-03-18 23:35:08','2019-03-18 23:35:08'),(4,'Dasky Villa','ingdasky@gmail.com',NULL,'$2y$10$fucYDImS/Qj2ecHnUZCfO.F00pZTiWMrcdJzAZGOVd/DNy0M41jaO',NULL,NULL,NULL,NULL,'2019-03-19 00:05:29','2019-03-19 00:05:29'),(5,'Luxama','luthsonluxa@yahoo.fr',NULL,'$2y$10$mZO078UYAnV822MNfITRgemMR6GqoIQTE6uwoifvuIsCcqwo0kOOi',NULL,NULL,NULL,NULL,'2019-03-19 00:11:28','2019-03-19 00:11:28'),(6,'Lucien Mardoché','inglucien@gmail.com',NULL,'$2y$10$.ufxs5BkHAcvSnTdQCjwe.GvvpJBbNs.zBXI4fw1TUDi5r.SU6W5O',NULL,NULL,NULL,NULL,'2019-03-19 01:09:37','2019-03-19 01:09:37'),(7,'Chawa pete','bradleybertrand13@yahoo.fr',NULL,'$2y$10$CnuuHWcoVWC2fzKqaIRCrOnwpY1gro9F01uG2fewC9pelL7/Buu5G',NULL,NULL,NULL,NULL,'2019-03-19 01:17:09','2019-03-19 01:17:09'),(8,'Tabie Bella','bellunetabithamegane@gmail.com',NULL,'$2y$10$B826S55VVDZBYke9yc.XguIi6z/kKCMdk3JsaxLui/BKOuNKA0ph.',NULL,NULL,NULL,NULL,'2019-03-19 03:14:04','2019-03-30 00:48:48'),(9,'Bertholin Ambroise','ambroisebertholin455@gmail.com',NULL,'$2y$10$G4BF/WSS50vgNrmejbcISennCRSO0kJkg3FMvUzXwFmRhpEFIUYpC',NULL,NULL,NULL,NULL,'2019-03-19 13:30:50','2019-03-19 13:30:50'),(10,'Kevin Andre','andrekevin1495@gmail.com',NULL,'$2y$10$F69InJ4GpQXpLPCRI6WaEeCb/.mxwWyYgD6iOyM.B.Tagoc1CqxW6',NULL,NULL,NULL,'qAPm8pLxPebv354MNoBQE2UQxuUFXZiGpGuNmAAF1yjCmsyPsJao43kBe6v0','2019-03-19 19:25:44','2019-03-19 19:25:44'),(11,'Chandelet','chandeletdoris@yahoo.fr',NULL,'$2y$10$VpHFVhsWDDCbSsnoMNVPMOWJApBjYdcDmQcWCYjbGukembgYxJaLW',NULL,NULL,NULL,NULL,'2019-03-19 22:46:39','2019-03-19 22:46:39'),(12,'administrateur','ashleyphili@gmail.com',NULL,'$2y$10$wbCpaAmp1ijGUF1y2Pbw6O2SGnW2YmStNXmL3P999IciBl1DvAFo6',NULL,NULL,NULL,NULL,'2019-03-19 22:50:18','2019-03-19 22:50:18'),(13,'Alexis Willinx','alexiswillinx@yahoo.fr',NULL,'$2y$10$YuZb3rAanZF.wD9B8UazVOtp2sn4i118SAC/6N/4MlJS6P0utS22O',NULL,NULL,NULL,NULL,'2019-03-19 23:47:16','2019-03-19 23:47:16'),(14,'Alex','aaltidort@yahoo.com',NULL,'$2y$10$.4tjmv6obCDDQr2VsdVcUeN3/YXyNAAp0dD60qoM4oqEA1rvDrmCC','(509)43-04-6328','005-594-656-0','82, Rue Borno Petion-ville','ycBF6MqT54L46xd3AGnF2N4rGdKhoMXEaFurvtI1UWA7ZbIa7L63bmbBUZfN','2019-03-20 04:08:48','2019-03-31 22:09:11'),(15,'Point du Jour Elso','pdjelso@gmail.com',NULL,'$2y$10$82BF7P4dX.8IbCjMo3CZ3uvnSUS2qYY0wDhLZEicQEvWGUrNeyrU6',NULL,NULL,NULL,NULL,'2019-03-20 15:46:12','2019-03-20 15:46:12'),(16,'Olsen Denis','dony48@gmail.com',NULL,'$2y$10$ovmXu4tUzcwkxOUmFraWV.0OGWAtVLDv/YzAGM1H9cCIyUhHpgLxi',NULL,NULL,NULL,NULL,'2019-03-20 16:30:44','2019-03-20 16:30:44'),(17,'Jean kemy Morose','morosejeankemy74@gmail.com',NULL,'$2y$10$.TwC2pyVFDOyIMOfESQxvuSOKF4JWrHocfuHM5YEwTAxPR.jR5RwG',NULL,NULL,NULL,NULL,'2019-03-20 16:40:03','2019-03-20 16:40:03'),(18,'AJL','Ajl@gmail.com',NULL,'$2y$10$6WuDWHUMm6/XRk4mhm54segWojRvbkaDmz7OkrNS5dye3p0szz.gS',NULL,NULL,NULL,NULL,'2019-03-20 16:50:29','2019-03-20 16:50:29'),(19,'Pierre-saint Nickelson','nickelsonpierresaint@gmail.com',NULL,'$2y$10$nWMoJ1G5yEB0NyOtuoZkNO4VP6jZZtMc2F3/FYC.4kKRlIr8893GC',NULL,NULL,NULL,NULL,'2019-03-20 17:38:42','2019-03-20 17:38:42'),(20,'ajl','ajl@ggmail.com',NULL,'$2y$10$RbIdlseQv1BnViqcgAOCGO50P1bObcosZg2BA0Fb7Gm5MUjekULN.',NULL,NULL,NULL,NULL,'2019-03-21 01:24:53','2019-03-21 01:24:53'),(21,'Teph Gab','stephgabo@gmail.com',NULL,'$2y$10$kjg8XtU1M2xNS/C5p.ZHOO8ec/tYn5Q1euipVHTxn8YHKO/D8yPa.',NULL,NULL,NULL,'s1kZM2P7mFPK9rSJN69SUuVCjHWwrhDWDB10t474fYCh9P6GbVTuoGq827dx','2019-03-21 02:13:50','2019-03-28 21:23:28'),(22,'Marc','mitchymarc@gmail.com',NULL,'$2y$10$l1wr07AwQVmaiMs5pm.pnO2yx7fb5d5GuWsWVs8AngzDz3cG9Z1DS',NULL,NULL,NULL,NULL,'2019-03-21 14:49:59','2019-03-21 14:49:59'),(23,'Teph Gab2','fffttt@gmail.com',NULL,'$2y$10$lwVCgzQK.yupPVNlm0eOK.nqsUK2Qy1GS4DW6QovAL4iHiSzsnU0K',NULL,NULL,NULL,NULL,'2019-03-21 18:16:33','2019-03-28 21:23:50'),(24,'Pierre','pierresallie@gmail.com',NULL,'$2y$10$CMSzI5d959/aMcTLcVYcgeVxJt8/EA.6P6dHue.K1O5U6QHB688PS',NULL,NULL,NULL,NULL,'2019-03-21 22:33:09','2019-03-21 22:33:09'),(25,'Ashley Phili','ashleyphili202@gmail.com',NULL,'$2y$10$q74bFlmvmD/ENsIQ37XgNOSE34aF9rkWaUYw2WeEXr2E9PVQt6G0y',NULL,NULL,NULL,NULL,'2019-03-22 02:26:23','2019-03-22 02:26:23'),(26,'jordan','jordanmondeseme@gmail.com',NULL,'$2y$10$6X1JMmKuGyXpXeCgQCZXB.OrDgGUlejim19TXz31YU/WUc4eBP6S6',NULL,NULL,NULL,NULL,'2019-03-23 05:46:25','2019-03-23 05:46:25'),(27,'Phylly','phylphylly@gmail.com',NULL,'$2y$10$NjP9wgqbQfv4gRVSH9Cea.Ushsy2SpR7LeiEL8PjKENw31OwpFOUO',NULL,NULL,NULL,NULL,'2019-03-27 15:35:20','2019-03-27 15:35:20'),(28,'Grégory','slowpen85@gmail.com',NULL,'$2y$10$BlJ6hl55oK0PuXmn9D1ppeooqXaqwhoE.wzyQ6fPR4RU1AmO3DQQm',NULL,NULL,NULL,NULL,'2019-03-30 01:41:08','2019-03-30 01:41:08'),(29,'Pierre gregory','pierregreg00@gmail.com',NULL,'$2y$10$.7kVv3Q7Z52d5SOTfDZbfelUWbMd9tRBhoFilmv3YN5KtRe1ZeIFS',NULL,NULL,NULL,NULL,'2019-03-31 00:36:33','2019-03-31 00:36:33'),(30,'Agella Dasten','dasten007@gmail.com',NULL,'$2y$10$GhCqdcMOubgSOCLcZlsLDeh7MTSD4Emr3n6MJ4fVNWK0weSflKpHS',NULL,NULL,NULL,NULL,'2019-04-02 17:42:58','2019-04-02 17:42:58'),(31,'Romain Kenny','rokennyj3623@gmail.com',NULL,'$2y$10$CebbwV1A2kC2noF8bM/m0ursRrxV/iNzNbR.1qArnA8BbV6JvwLLO',NULL,NULL,NULL,NULL,'2019-04-03 13:30:25','2019-04-03 13:30:25'),(32,'Mentor','machenrymentor12@gmail.com',NULL,'$2y$10$FnP/lOpY62srndEqune3hultjdp2NYmJdgQe/tz27aL6pltU9cS1q',NULL,NULL,NULL,NULL,'2019-04-03 21:20:11','2019-04-03 21:20:11'),(33,'Stan','bonbgy23@gmail.com',NULL,'$2y$10$vb1hyXQifBfdrMtvA0slVOhbq7tDvfeGjMxTMX/kV6/zXIF8/0ulC',NULL,NULL,NULL,NULL,'2019-04-04 00:56:59','2019-04-04 00:56:59'),(34,'Jean Daniel','rodriguejeandanielpierre@gmail.com',NULL,'$2y$10$riIWjOm/L85Dg5ZsxSpRjeN/QQNh4eexz0Jq.LEhlhbW5X3qIsbqG',NULL,NULL,NULL,NULL,'2019-04-04 20:04:55','2019-04-04 20:04:55'),(35,'Milfort Stanley','ms123@yahoo.fr',NULL,'$2y$10$knNVBo5zx4TwXrLVgBkAQ.D5j6GbxqYOez/Hj8HSur7rnxUiIwuGW',NULL,NULL,NULL,NULL,'2019-04-07 19:06:35','2019-04-07 19:06:35'),(36,'Pierre-Yvens Cassamajor','sabatinocassamanolo1992@gmail.com',NULL,'$2y$10$SKwMgaqLLqCwdvrienec1uh8PpTmLEcwi2lBCUfGg1ZD6uCiSHJzC',NULL,NULL,NULL,NULL,'2019-04-08 03:12:23','2019-04-08 03:12:23'),(37,'test-category','test@gmail.com',NULL,'$2y$10$TgBeGWordQ6U7DKbXwoWEuXs9gwrWEXFk7oqVN1CdL2d5YjX3vjyu',NULL,NULL,NULL,NULL,'2019-04-08 04:24:24','2019-04-08 04:24:24'),(38,'ambroise','ambroisebertholin@yahoo.fr',NULL,'$2y$10$/amA6.7/fkISH3ebXlPZOux3UpQFRLMaDAKHpRIh5iQFfilA3xNWi',NULL,NULL,NULL,NULL,'2019-04-08 16:14:26','2019-04-08 16:14:26'),(40,'Yve-L  Art','Yve-LArt@yahoo.com',NULL,'$2y$10$nE8E61FrR0OMC4U5/9BQy.EbRsIQf3pBpfyViQf3v1kJsA0I.3r9u',NULL,NULL,NULL,NULL,'2019-04-09 03:45:49','2019-04-09 04:15:08'),(41,'Chassagne','donachassagne80@gmail.com',NULL,'$2y$10$n19N26DVxsXgq9rt7x7Ub.wmXNf4aHPrXROyzxX1bxV4u198rT5O.',NULL,NULL,NULL,NULL,'2019-04-10 00:14:04','2019-04-10 00:14:04'),(42,'Darline Pierre','dahduhpierre@yahoo.ht',NULL,'$2y$10$j3iYg0U.MyYq0NB91xr1zOyqsG5fcV38XFejl7wn44ei129kyvbfy',NULL,NULL,NULL,NULL,'2019-04-11 17:46:24','2019-04-11 17:46:24'),(43,'belizaire angelo','belizaireangelo@yahoo.fr',NULL,'$2y$10$YuhK3gVc8HrJL2U9dceT.uFF7wnQMxazBVbv7zAITdi8Fhoz1x6mO',NULL,NULL,NULL,NULL,'2019-04-12 18:47:16','2019-04-12 18:47:16'),(44,'Antoine Drean','claire19@yahoo.fr',NULL,'$2y$10$VZXp9zTo0z6RaLvGBpms9.kzmNstpOX44BS4yt7ovyieltlnorVe2',NULL,NULL,NULL,NULL,'2019-04-12 20:06:19','2019-04-12 20:44:11'),(45,'makenzy','moy3monroe@yahoo.fr',NULL,'$2y$10$JoWpbDd3iiRn5oS507dFz.IEi58prfm.UNGAf5NJS79qP0KxreA2O',NULL,NULL,NULL,NULL,'2019-04-14 23:48:32','2019-04-14 23:48:32'),(46,'YVenold LUCIEN','Yve-L.Art@yahoo.com',NULL,'$2y$10$BoemYAbcs1hnqo3Rai3hvu1GJm7gy6YzkqyH21GcPDpb27NlZrBdG',NULL,NULL,NULL,NULL,'2019-04-18 03:05:14','2019-04-18 03:05:14'),(47,'Dona Chassagne','pj12465@gmail.com',NULL,'$2y$10$R6lWMcVnBi7UALKHlrKUVeONq5Wykt1k4Ml/h16Z539XVwtIoB.AC',NULL,NULL,NULL,NULL,'2019-04-28 14:45:05','2019-04-28 14:45:05'),(48,'Djoune Phili','www.ginette.mvl@gmail.com',NULL,'$2y$10$cR6Nt3B8efp3C32011W8/e/IyjcO5h7uwjIKw/X6Duc7WhrCJnLtO','50947316517 / 50941413741','0000','Delmas 31 rue Vernet',NULL,'2019-05-07 00:21:07','2019-05-07 02:41:37'),(49,'Ginette Phili','ginette.mvl@gmail.com',NULL,'$2y$10$dDLZggxw0KoGDSBdqY5Lf.Bs/CL7aKD52qlvyfVWopj4NMGM4iYzS','41413741','1111','Delmas 31 rue Vernet #8',NULL,'2019-05-07 00:48:05','2019-05-07 02:23:38'),(50,'Bòz\'or','kevensky93@gmail.com',NULL,'$2y$10$Ndg7EDEmsbkVGWYHd1xmo.GAKxt02.3hH/nF8.adkPoZqM6go92qe',NULL,NULL,NULL,NULL,'2019-05-28 19:27:21','2019-05-28 19:27:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-09 21:32:59
