<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcossasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecossas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eglise_id')->unsigned();
            $table->integer('classe_id')->unsigned();
            $table->string('directeur');
            $table->string('priere');
            $table->string('rapport');
            $table->string('meditation');
            $table->string('bulletin');
            $table->date('pour_le');
            $table->foreign('classe_id')->references('id')->on('classes')->onDelete('cascade');
            $table->foreign('eglise_id')->references('id')->on('eglises')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecossa');
    }
}
