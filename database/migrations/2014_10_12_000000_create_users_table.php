<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_complet')->nullable();
            $table->string('naissance')->nullable();
            $table->string('genre')->default('male');
            $table->string('telephone');
            $table->string('email', 100)->unique();
            $table->enum('role', ['user', 'admin','root'])->default('user');
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('eglise_id')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
