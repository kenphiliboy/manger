<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('annonce_id')->nullable();
            $table->unsignedInteger('eglise_id')->nullable();
            $table->unsignedInteger('distrie_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->boolean('status')->default('1');
            $table->text('departement', 20)->nullable();
            $table->text('message')->nullable();
            $table->date('date_n');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
