<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::rename('posts', 'commentaires');
        Schema::rename('anciennas', 'anciennats');
        Schema::rename('diacres', 'diaconats');
        Schema::rename('ecossas', 'ecosas');
        Schema::rename('cults', 'cultes');

        Schema::table('ecosas', function(Blueprint $table){
            $table->renameColumn('meditation', '5minutes');
            $table->renameColumn('directeur', 'directeur_trice');
        });

        Schema::table('laics', function(Blueprint $table){
            $table->renameColumn('directeur', 'directeur_trice');
        });

        Schema::table('musiques', function(Blueprint $table){
            $table->string('musicien')->after('id');
            $table->renameColumn('chant_special', 'chants_speciaux');
        });

        Schema::table('iterations', function(Blueprint $table){
           $table->integer('eglise_id')->unsigned()->after('id');
            $table->foreign('eglise_id')->references('id')->on('iterations')->onDelete('cascade');
        });

        Schema::table('cultes', function(Blueprint $table){
            $table->renameColumn('appelle', 'appel');
            $table->renameColumn('remerciment', 'benediction');
            $table->renameColumn('priere', 'priere_pastoral');
            $table->renameColumn('service', 'service_de_fidelite');
        });

        Schema::table('reunions', function(Blueprint $table){
            $table->renameColumn('remerciment', 'remerciement');
        });

        Schema::table('diaconats', function(Blueprint $table){
            $table->string('jour')->nullable()->after('groupe');
            $table->string('diacres_de_service')->nullable()->after('jour');
            $table->string('autres')->nullable()->after('diacres_de_service');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
/*        Schema::rename('anciennats','anciennas');
        Schema::rename('diaconats', 'diacres');
        Schema::rename('ecosas','ecossas');
        Schema::rename('cultes', 'cults');

        Schema::table('ecossas', function(Blueprint $table){
            $table->renameColumn('5minutes', 'meditation');
            $table->renameColumn('directeur_trice', 'directeur');
        });

        Schema::table('laics', function(Blueprint $table){
            $table->renameColumn('directeur_trice', 'directeur');
        });

        Schema::table('cults', function(Blueprint $table){
            $table->renameColumn('appel', 'appelle');
            $table->renameColumn('priere_pastoral', 'priere');
            $table->renameColumn('benediction', 'remerciment');
        });*/
    }
}
