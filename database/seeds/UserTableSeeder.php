<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'nom_complet' => 'Philibert kendy',
            'telephone' => '+50934112534',
            'email' => 'philiboy@gmail.com',
            'role' => 'root',
            'eglise_id'=> '1',
            'password' => bcrypt('Events@@2019!'),
            'email_verified_at' => Carbon::now(),
        ]);

        User::create([
            'nom_complet' => 'Ashley Phili',
            'telephone' => '+50934112535',
            'email' => 'phili@gmail.com',
            'role' => 'admin',
            'eglise_id'=> '1',
            'password' => bcrypt('code1234'),
            'email_verified_at' => Carbon::now(),
        ]);
    }
}
