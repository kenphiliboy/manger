<?php

use Carbon\Carbon;
use App\Models\Eglise;
use Illuminate\Database\Seeder;

class EgliseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Eglise::create([
            'nom' => 'Bethesda',
            'adresse' => 'Petion ville bois moquete',
            'localisation' => '001 2221',
            'user_login' => 'bethesda',
            'password' => bcrypt('code1234'),
            'statut' => '1',
            'user_id' => '1',
            'distrie_id' => '1',

        ]);
        
        Eglise::create([
            'nom' => 'Betanie',
            'adresse' => 'Petion ville St Therese',
            'localisation' => '001 2221',
            'user_login' => 'betanie',
            'password' => bcrypt('code1234'),
            'statut' => '1',
            'user_id' => '2',
            'distrie_id' => '1'
        ]);
        


    }

}
