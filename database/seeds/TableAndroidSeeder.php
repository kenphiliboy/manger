<?php

use Illuminate\Database\Seeder;
use App\Models\{
		Iteration,
		Classe,
		Ecossa,
		Laic,
		Cult,
		Reunion,
		Diacre,
		Jeunesse,
		Musique,
		Divers,
		Ancienna,
		Annonce,
		Groupe,
        Alert,
	};

class TableAndroidSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');

        $date =['2018-12-14', '2018-12-21','2018-12-28'];

        $models = ['Ecossa', 'Laic', 'Cult', 'Reunion', 'Diacre', 'Jeunesse', 'Musique', 'Divers'];

        $departement = ['Musique', 'Diaconna', 'Anciennat'];

        $clubs = ['Aventurier', 'Eclaireur', 'Segnor'];

        $iteration = new Iteration();

        $classe = new Classe();

        for($i = 0; $i < 3; $i++)
        {
	    
	        $groupe = new Groupe();

	        $ancienna = new Ancienna();
        
        	$iteration->status = 1;

        	$iteration->pour_le = $date[$i];

        	$groupe->departement = $departement[$i];

        	$groupe->nom = $faker->unique()->lastName();

        	for($y = 0; $y < 4; $y++){
        		$groupe->liste .= $faker->firstName().' '.$faker->lastName().', ';
	        	
	        	$ancienna->liste .= $faker->firstName().' '.$faker->lastName().', ';
        	}

        	$ancienna->eglise_id = 1;

        	$ancienna->pour_le = $date[$i];

        	$groupe->save();

        	$ancienna->save();

            $iteration->save();

        	foreach ($models as $key => $model) {

                if($model === 'Divers'){

                    foreach ($departement as $key => $value) {
                        # code...
                        $divers = new Divers();

                        $divers->departement = $value;
                        
                        $divers->info = $faker->realText($faker->numberBetween(150,300));

                        $divers->eglise_id = ($i+1);

                        $divers->pour_le = $date[$i];

                        $divers->save();

                        $divers = null;                       
                    }

                }else if($model === 'Jeunesse'){

                    foreach ($clubs as $key => $club) {
                        
                        $aventurier = new Jeunesse();
                        
                        $aventurier->club = $club;
                        
                        $aventurier->info = $faker->realText(300);
                        
                        $aventurier->eglise_id = ($i+1);
                        
                        $aventurier->pour_le = $date[$i];
                        
                        $aventurier->save();

                        $aventurier = null;
                    }

                }else{
                    $mod = $model;

                    for ($a=1; $a<4 ; $a++) { 

                        $model = 'App\Models\\'.$mod;

                        $model = new $model();
                            
                        $fil = $model->getFillable();

                        $data = [];
                       
                        foreach ($fil as $value) {
                            
                            if($value =='eglise_id' || $value == 'classe_id' || $value == 'jour' || $value == 'groupe')
                                $model->$value = ($i+1);
                            else if($value == 'pour_le'){
                                    $model->$value = $date[$i];
                            }
                            else
                                $model->$value = $faker->firstName().' '.$faker->lastName();

                        }

        	            $model->save();
                    }

                }

        	}

        }

        Alert::create([
            'annonce_id' => 3,
            'departement'=> 'laic',
            'message'=> "l'eglise vous salue cordialement dans le precieux nom de Jesus et profite de cette occatsion pour vous 
                         annonce que vous etes dans la liste des personnels pour la semaine veuillez s'il vous plait en cas 
                         d'incovenient nous faire part de votre abscence motive le au moin un jour avant la date prevu et vous 
                         souhaite par la meme occaion une greable semaine dans le precieux nom de Jesus."
        ]);


    }
}
