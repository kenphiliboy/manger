<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ClassesTableSeeder::class);
        $this->call(DistrieTableSeeder::class);
        $this->call(EgliseTableSeeder::class);
        $this->call(GroupeTableSeeder::class);
        //$this->call(AnnonceTableSeeder::class);
        //$this->call(TableAndroidSeeder::class);
        
    }
    
}
