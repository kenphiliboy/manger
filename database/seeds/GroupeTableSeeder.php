<?php

use Illuminate\Database\Seeder;
use App\Models\Groupe;

class GroupeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Groupe::create([
    		'departement'=>'diaconna',
    		'nom'=> 'A',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'diaconna',
    		'nom'=> 'B',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'diaconna',
    		'nom'=> 'C',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'diaconna',
    		'nom'=> 'D',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'musique',
    		'nom'=> '1',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'musique',
    		'nom'=> '2',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'musique',
    		'nom'=> '3',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
    	Groupe::create([
    		'departement'=>'musique',
    		'nom'=> '4',
    		'liste'=> 'Jeune Manse - Loudi Emma - Charle Frederieque',
    	]);
        
    }
}
