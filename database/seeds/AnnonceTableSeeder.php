<?php

use Illuminate\Database\Seeder;
use App\Models\Annonce;

class AnnonceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Annonce::create([
        	'statut' => '1',
        	'pense' => 'L\'home n\'est grand qu\'a genoux',
        	'pour_le' => '2018-12-04',
        	'eglise_id' => '1',
        ]);
        Annonce::create([
        	'statut' => '1',
        	'pense' => 'La liberte est le pouvoir ',
        	'pour_le' => '2018-12-28',
        	'eglise_id' => '1',
        ]);
        Annonce::create([
        	'statut' => '0',
        	'pense' => 'La vie est un cadeau et la mot un repos',
        	'pour_le' => '2018-11-19',
        	'eglise_id' => '2',
        ]);
        Annonce::create([
        	'statut' => '1',
        	'pense' => 'il suffit de regarder devant soit',
        	'pour_le' => '2018-12-7',
        	'eglise_id' => '2',
        ]);
        Annonce::create([
        	'statut' => '0',
        	'pense' => 'Jesus nous aime tous',
        	'pour_le' => '2018-12-14',
        	'eglise_id' => '2',
        ]);
        Annonce::create([
        	'statut' => '0',
        	'pense' => 'La vie est courte et le jugement inevitable',
        	'pour_le' => '2018-12-21',
        	'eglise_id' => '1',
        ]);
    }
}
