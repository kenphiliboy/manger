<?php

use Illuminate\Database\Seeder;
use App\Models\Classe;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Classe::create([
        	'ministere' => 'Eglise enfantine',
        ]);

         Classe::create([
        	'ministere' => 'Cadet',
        ]);

          Classe::create([
        	'ministere' => 'Grande eglise',
        ]);

    }

}
