<?php

use Illuminate\Database\Seeder;
use App\Models\Distrie;

class DistrieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Distrie::create([
        	'user_id' => '1',
            'secteur' => 'secteur 7',
            'eglise_mere' => 'Bethesda',
            'partie_de' => 'federation',
            'nom' => 'Malie de Kenskoft',
            'responsable' => 'Obed Ville',
        ]);

        Distrie::create([
            'user_id' => '2',
            'nom' => 'Jacquet',
            'partie_de' => 'mission',
            'secteur' => 'secteur 5',
            'eglise_mere' => 'Betanie',
            'responsable'=> 'Charles Ymma',
            
        ]);

    }

}
