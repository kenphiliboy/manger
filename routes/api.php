<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['namespace'=>'API', 'prefix'=> '/app/', 'middleware'=>'auth:api'],function(){
    Route::get('iteration', 'IterationController@getIteration');
    Route::get('annonce/{from}/{to}', ['uses' =>'AnnonceController@getAnnonceByDate']);
    Route::get('user', 'AuthController@user');
    Route::post('annonces', 'AnnonceController@save');
    Route::get('annonce/list', 'AnnonceController@listUpdate');
    Route::put('annonces/{id}', 'AnnonceController@update');
    Route::get('annonce/publish', 'AnnonceController@listPublish');
    Route::put('annonce/publish/{id}', 'AnnonceController@publish');
    
});

Route::group(['prefix' => '/app/auth/','namespace'=> 'API'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::get('token', 'AuthController@newToken');
        Route::put('update', 'AuthController@updateProfil');
        Route::put('update_pass', 'AuthController@update_password');

    });
});

Route::group(['prefix'=> '/app/', 'namespace'=> 'API'], function() {
	
	Route::post('log/data', 'AuthController@getDataRegister');

});
