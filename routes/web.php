<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([],function(){
	Route::get('eglise.log', 'HomeController@log')->name('eglise.log');
	Route::get('android', 'HomeController@download')->name('apk');
});

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::group(['middleware'=>'auth:eglise', 'namespace'=>'Admin'], function(){

	Route::get('/', 'AdminController@index');
	Route::resource('groupe', 'GroupeController');
	Route::resource('annonce', 'AnnonceController');
	Route::put('annonce.state/{id}', 'AnnonceController@changeState')->name('annonce.state');
});

Route::group(['namespace'=>'Admin'], function(){
	Route::post('admin/log', 'AdminController@logAdmin')->name('admin.log');
});

Route::group(['middleware'=>'auth', 'namespace'=>'Admin'], function(){
		Route::get('admin', 'AdminController@index')->name('admin.index');
	  Route::resource('eglise', 'EgliseController');
});

Route::group(['middleware'=>'auth'], function(){
	Route::resource('district', 'District\DistrictController');
});

Route::resource('users', 'Users\UsersController');

Route::resource('roles', 'Users\RolesController');

Route::resource('permissions', 'Users\PermissionsController');
