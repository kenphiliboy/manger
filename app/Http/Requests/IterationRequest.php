<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IterationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status'=>                        'required|string',
            'pour_le'=>                       'required|string',
            'changed'=>                       'required|string',
            'eglise_id'=>                     'required|string',
        ];
    }
}
