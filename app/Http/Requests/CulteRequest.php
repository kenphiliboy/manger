<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CulteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       
        return [
            'bienvenue'=>           'required|string|',
            'appel'=>               'required|string|',
            'lecture'=>             'required|string|',
            'priere_pastorale'=>    'required|string|',
            'predication'=>         'required|string|',
            'benediction'=>         'required|string|',
            'service_de_fidelite'=> 'required|string|',
            'pour_le'=>             'required|date|',
            'eglise_id'=>           'required|string|',
            'classe_id'=>           'required|string|',

        ];
    }
}
