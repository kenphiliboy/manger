<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AllRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];
        $dept = !empty($this->model) ? $this->model : 'Annonce';

        $source = '\App\Http\Requests\\'.ucfirst($dept).'Request';
        $source = new $source();
        $rules = $source->rules();


        return $rules;
    }
}
