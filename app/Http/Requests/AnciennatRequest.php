<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnciennatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'liste'=>       'required|string|min:10',
            'pour_le'=>     'required|date',
        ];
    }
}
