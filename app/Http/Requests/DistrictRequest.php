<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistrictRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>             'required|string',
            'nom'=>                 'required|string',
            'secteur'=>             'required|string',
            'responsable'=>         'required|string',
            'partie_de'=>           'required|string',

        ];
    }
}
