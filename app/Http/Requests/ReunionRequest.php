<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReunionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eglise_id'=>            'required|string',
            'pour_le'=>              'required|string',
            'jour'=>                 'required|string',
            'bienvenue'=>            'required|string',
            'remerciement'=>          'required|string',
            'meditation'=>           'required|string',
        ];
    }
}
