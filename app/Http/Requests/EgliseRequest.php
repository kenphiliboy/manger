<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EgliseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom'=>                     'required|string',
            'adresse'=>                 'required|string',
            'status'=>                  'required|string',
            'localisation'=>            'string',
            'user_id'=>                 'required|string',
            'user_login'=>              'required|string',
            'password'=>                'required|string',
            'distrie_id'=>              'required|string',
        ];
    }
}
