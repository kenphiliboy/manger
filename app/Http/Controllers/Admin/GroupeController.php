<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Groupe;

class GroupeController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupes = Groupe::all();

        return view('groupe.index')->withGroupes($groupes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('groupe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'departement'=> 'required|string',
            'nom'=> 'required|string',
            'liste'=> 'required|string',
        ]);

        $data = $request->only('departement', 'nom', 'liste');

        Groupe::create($data);

        return redirect()->route('groupe.index')->with('ok', __("Le nouveu groupe est disponible pour le departement $request->departement"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $groupe = Groupe::find($id);

        return view('groupe.show')->withGroupe($groupe);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupe = Groupe::find($id);

        return view('groupe.edit')->withGroupe($groupe);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupe = Groupe::find($id);

        if($groupe->id == $request->get('variation'))
        {
            $groupe->update($request->only('nom', 'departement', 'liste'));

            return redirect()->route('groupe.index')->with('ok', __('La mise a jour est effectue avec succes'));
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupe = Groupe::find($id);

        $groupe->delete();

        return redirect()->route('groupe.index')->with('ok', __('Le groupe est supprime avec succes'));
    }
}
