<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Classe;
use App\Models\Ecossa;
use App\Models\Laic;
use App\Models\Cult;
use App\Models\Reunion;
use App\Models\Diacre;
use App\Models\Jeunesse;
use App\Models\Musique;
use App\Models\Divers;
use App\Models\Ancienna;
use App\Models\Annonce;
use App\Models\Groupe;
use App\Models\Iteration;
use Carbon\Carbon;

class AnnonceController extends Controller
{
    protected $_data;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*public function __construct()
    {
        $this->middleware('ajax', ['only'=> 'store']);
    }*/
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $annonces = Annonce::where('eglise_id', auth()->user()->id)->get();

        return view('annonce.index')->withAnnonces($annonces);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = Classe::all();

        $diaconna = Groupe::where('departement','diaconna')->get();

        $musique = Groupe::where('departement','musique')->get();

        return view('annonce.create', compact('classes','diaconna', 'musique'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!empty($request->get('model'))){
                   
            $class = 'App\Models\\'.ucfirst($request->get('model'));

            $tab = new $class();

            $fil = $tab->getFillable();

            $data = [];

            foreach ($fil as $value) {
                
                if($fil =='eglise_id')
                    $data[$value] = (int)$request->get($value);
                else
                    $data[$value] = $request->get($value);
            }

            $class::create($data);

            if($request->get('model') == 'annonce')
                Interation::create([
                    'pour_le'=> $request->get('pour_le'),
                    'status'=> '0',
                    'changed'=> '0',
                ]);

            $reponse = ['status'=> '200', 'msg'=> 'insertion reussie'];

            return \Response::json($reponse);   
            
        }else
            return \Response::json(['status'=> '500', 'msg'=> 'Le model n\'est pa renseigne dans cette requette']);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        die("show method");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classes = Classe::all();

        $diaconna = Groupe::where('departement','diaconna')->get();

        $musique = Groupe::where('departement','musique')->get();

        $annonce = Annonce::where('id', $id)->where('eglise_id', \Auth::user()->user_id)->get()[0];

        $data = $this->getAnnonceByDate($annonce->pour_le);

        foreach ($this->_data as $key => $value) {
            $temp[$key] = ($value instanceof \Illuminate\Database\Eloquent\Collection) ? $value->first() : $value;
        }

        $temp['data'] = $data;

        $temp['annonce'] = $annonce;

        $temp['musique'] = $musique;

        $temp['diaconna'] = $diaconna;

        $temp['classes'] = $classes;

        return view('annonce.edit', $temp);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Annonce $annonce)
    {
        $annonce = Annonce::findOrFail($annonce)->first();

        $classe_id = !empty($request->get('classe_id')) ? $request->get('classe_id') : 0;

        $jour = !empty($request->get('jour')) ? $request->get('jour') : 0;

        $club = !empty($request->get('club')) ? $request->get('club') : 0;

        if(!empty($request->get('model'))){

            Iteration::where('pour_le', $annonce->pour_le)->update(['status'=> '0', 'changed'=> '1']);
                   
            $model = 'App\Models\\'.ucfirst($request->get('model'));

            $temp=[];

            $instance = strtolower($request->get('model'));

            switch ($instance) {
                case 'ecossa':
                    $temp = $model::where('pour_le', $annonce->pour_le)
                    ->where('classe_id', $classe_id)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'laic':
                    $temp = $model::where('pour_le', $annonce->pour_le)
                    ->where('classe_id', $classe_id)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'cul':
                    $temp = $model::where('pour_le', $annonce->pour_le)
                    ->where('classe_id', $classe_id)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'ancienna':
                    $temp = $model::where('pour_le', $annonce->pour_le)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'musique':
                    $temp = $model::where('pour_le', $annonce->pour_le)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'reunion':
                    $temp = $model::where('jour', $jour)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'jeunesse':
                    $temp = $model::where('club', $club)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'divers':
                    $temp = $model::where('club', $club)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'annonce':
                    $annonce->penser = $request->get('penser');
                    $annonce->save();
                    break;
                default:
                    Iteration::where('pour_le', $pour_le)->update(['changed'=> '0']);
                    $reponse = ['status'=> '200', 'msg'=> 'Departement '.$instance.'non retrouver'];
                    return \Response::json($reponse);
                    break;
            }

            $annonce->statut = 0;

            $annonce->save();
            
            $reponse = ['status'=> '200', 'msg'=> 'Modification reussie', 'data'=>$temp];

            return \Response::json($reponse);   
            
        }else
            return \Response::json(['status'=> '404', 'msg'=>'Departement non retrouver', 'request'=> $request->all()]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeState(Request $request, $id)
    {
        $annonce = Annonce::findOrFail($id);

        $disponible = $request->get('statut_update');

        if($disponible == 0){
            
            Iteration::where('pour_le', $annonce->pour_le)->update(['status'=> '1']);

            $annonce->statut = 1;

        }else{

           /* Iteration::where('pour_le', $annonce->pour_le)->update(['status'=> '0']);

            $annonce->statut = 0;*/
        }

        $annonce->save();

        return redirect()->route('annonce.index')->with('ok', 'Chagement de statut effectuer');


    }

    public function getAnnonceByDate($str)
    {
        $models = ["Classe","Ancienna","Diacre","Ecossa","Laic","Cult","Jeunesse","Musique","Reunion","Divers","Groupe"];
        
        $from = new Carbon($str);

        $limit = new Carbon($str);

        $limit->addDay(6);

        $data = [];

        foreach ($models as $key => $model) {
            if($model === "Classe" || $model === "Groupe"){
                $model1 = "App\Models\\".$model;
                $data[$model] = $model1::all();                
            }else{
                $model1 = "App\Models\\".$model;
                $data[$model] = $model1::whereBetween('pour_le', [$from, $from])->orderBy('pour_le')->get();
                
            }

            if(empty($data[$model]))
                unset($data[$model]);
        }

        foreach ($data as $key => $value) {
            if(count($value) == 1)
                $data[$key] = $value[0];
        }

        $this->_data = $data;

        return \Response::json($data, 200);
    }
    
}
