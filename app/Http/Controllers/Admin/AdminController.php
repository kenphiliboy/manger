<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminController extends Controller
{
	use AuthenticatesUsers;

	protected $username = 'user_login';

	
	public function __construct()
	{
		$this->middleware('auth')->except('logAdmin');
	}

    public function index()
    {
    	return view('admin.index');
    }

	public function logAdmin(Request $request)
	{
		//validation du formulaire
		$request->validate([
			'user_login' => 'required|string',
			'password' => 'required|min:4'
		]);

		$data = $request->only('user_login','password');

		//authentification 
		if( Auth::guard('eglise')->attempt($data, $request->get('remember')) ){
			return redirect()->intended(route('admin.index'));
		}

		return redirect('eglise.log');

	}

	public function logOutAdmin()
	{
		Auth::guard('eglise')->logout();
		
		return redirect('eglise.log');		  

	}



}
