<?php

namespace App\Http\Controllers\District;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Distrie;

class DistrictController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distries = Distrie::where('user_id', auth()->user()->id)->get();

        return view('district.index', compact('distries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('district.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validData = $request->validate([
            'nom' => 'required|string|max:50',
            'secteur' => 'required|numeric|digits_between:1,7',
            'partie_de' => 'required|string',
            'responsable'=> 'required|string',
            /*'user_id' => 'required|exists:users,id|unique:users,id',*/
        ]);

        Distrie::create([
            'user_id' => auth()->user()->id,
            'nom' => $request->get('nom'),
            'secteur' => $request->get('secteur'),
            'partie_de' => $request->get('partie_de'),
            'eglise_mere' => $request->get('eglise_mere'),
            'responsable' => $request->get('responsable'),
        ]);

        return redirect()->route('district.index')
                        ->with('ok','La distrie est ajoute felicitation maintenant vous pouvez ajouter les eglises'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $distrie = Distrie::find($id);

        return view('district.show', compact('distrie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distrie = Distrie::findOrFail($id);

        return view('district.edit')->withDistrie($distrie);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $distrie = Distrie::findOrFail($id);

        $input = $request->only('nom','secteur','eglise_mere','partie_de');

        $distrie->update($input);

        return redirect()->route('district.index')->with('ok', __('La mise a jour est termine avec succes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
        Distrie::find($id)->delete();

        redirect()->route('home')->with('ok', __('Suppression reussie la distrie a bien ete supprime'));
    }

}
