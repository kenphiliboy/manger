<?php

namespace App\Http\Controllers\District;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eglise;
use App\Models\Distrie;
use \Hash;


class EgliseController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eglises = Eglise::where('user_id', auth()->user()->id)->get();

        return view('eglise.index', compact('eglises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $distries = Distrie::where('user_id', auth()->user()->id)->get();

        return view('eglise.create', compact('distries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nom' => 'required|string|max:50',
            'adresse' => 'required|string|max:50',
            'responsable' => 'required|string|max:50',
            'avatar' => 'required|string|max:50',
            'password' => 'required|string|min:6',
            'distrie_id' => 'required',
            /*'user_id' => 'required|exists:users,id|unique:users,id',*/
        ]);

        Eglise::create([ 
            'nom' => $request->get('nom'),
            'adresse' => $request->get('adresse'),
            'responsable' => $request->get('responsable'),
            'user_id' => auth()->user()->id,
            'avatar' => $request->get('avatar'),
            'password' => Hash::make($request->get('password')),
            'distrie_id' => (int)$request->get('distrie_id'),
        ]);

        return redirect()->route('eglise.index')
            ->with('ok','Une eglise est ajoute a votre liste felicitation');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eglise = Eglise::find($id);

        $distries = Distrie::where('user_id', auth()->user()->id)->get();

        return view('eglise.edit', compact('eglise', 'distries'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Eglise $eglise)
    {
        
        $eglise->nom = $request->get('nom');

        $eglise->adresse = $request->get('adresse');

        $eglise->responsable = $request->get('responsable');

        $eglise->avatar = $request->get('avatar');

        if(!empty($request->get('ok')) && $request->get('ok') == 'on')
            $eglise->password = Hash::make($request->get('password'));

        $eglise->distrie_id = $request->get('distrie_id');

        $eglise->save();

        return redirect()->route('eglise.index')
                    ->with('ok','La mise a jour est effectue avec succes');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eglise $eglise)
    {
        $eglise->delete();

        return redirect()->route('eglise.index')
                    ->with('ok','La suppression est acheve avec succes');
        
    }
}
