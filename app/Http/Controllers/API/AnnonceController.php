<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\AllRequest;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\ {
        Alert,
        Iteration,
        Classe,
        Ecossa,
        Etude,
        Laic,
        Cult,
        Reunion,
        Diacre,
        Jeunesse,
        Musique,
        Divers,
        Ancienna,
        Annonce,
        Groupe,
        Distrie,
        Eglise,
    };

class AnnonceController extends Controller
{
    private $alerts = [];

    public function getAnnonceByDate($from, $to)
    {

    	$models = ["Classe","Anciennat","Diaconat","Ecosa","Laic","Culte","Jeunesse","Musique","Reunion","Divers","Groupe","Etudes"];

        $annonces = [];
        
        $values = Annonce::whereBetween('pour_le', [$from, $to])->get();

        foreach ($values as $key => $value) {
            
            $annonces[$key] = $value;

            $annonces[$key]->data = json_encode($this->getData($models, $value->pour_le), true);

        }

        $result = [
            'status'=> 200,
            'annonces'=> $annonces,
            'alerts'=> $this->alerts
        ];

        return response()->json($result);
    }

    private function getData($models, $date)
    {
        $from = Carbon::parse($date);
        $limit = Carbon::parse($date);
        $from->subDay(6);
        $data = [];
        $temp = '';

        foreach ($models as $key => $model) {
            if($model === "Classe" || $model === "Groupe"){
                $model1 = "App\Models\\".$model;
                $data[$model] = $model1::all();                
            }else{
                $model1 = "App\Models\\".$model;
                $data[$model] = $model1::whereBetween('pour_le', [$from, $limit])->orderBy('pour_le')->get();
                
            }

            if(empty($data[$model]))
                unset($data[$model]);
            
            $nbre;
            
            $temp = json_encode($data[$model], true);

            str_replace(\Auth::user()->nom_complet, \Auth::user()->nom_complet, $temp, $nbre);

            if($nbre > 0){
                $this->alerts[] = [
                    'date_n'=> ''.$date,
                    'departement'=>$model,
                    'statut'=> '1',
                    'message'=> 'Vous faites partie des personnes ayant pour la semaine une tache a accomplir dans la vigne du Seigneur',
                ];
                
            }
            
        }

        return $data;

    }

    public function save(AllRequest $request)
    {
        $jours = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
         
        if(!empty($request->get('model'))){
                   
            $class = 'App\Models\\'.ucfirst($request->get('model'));

            $tab = new $class();

            $fil = $tab->getFillable();

            $data = [];

            //return \Response::json($request->all(), 422);

            foreach ($fil as $value) {

                if($value =='eglise_id')
                    $data[$value] = (int)$request->get($value);
                else if($value == 'classe_id'){

                    if(is_null(Classe::where('ministere', $request->get('classe_id'))->get()->first())){
                
                        $temp = Classe::all();

                        $resp = 'Nom disponible: ';
                        
                        foreach ($temp as $key => $value) {
                           $resp .= $value->ministere.' ,';
                        }

                        $reponse = ['message'=> $resp];

                        return \Response::json($reponse, 422);

                    }

                    $data[$value] = Classe::where('ministere', $request->get($value))->get()->first()->id;
                
                }else if($value == 'groupe'){
                    
                    if(is_null(Groupe::where('nom', $request->get('groupe'))->get()->first())){
                    
                        $reponse = ['message'=> "ce groupe n'existe pas"];
                    
                        return \Response::json($reponse, 422);
                    
                    }
                    
                    $data[$value] = Groupe::where('nom', $request->get($value))->get()->first()->id;
                
                }else if($value == 'jour')
                    $data[$value] = (array_search(ucfirst($request->get('jour')), $jours) + 1);              
                else
                    $data[$value] = $request->get($value);
            }

            $class::create($data);

            if(strtolower($request->get('model')) == 'annonce')

            $reponse = ['code'=> '200', 'message'=> 'insertion reussie'];

            return \Response::json($reponse);       
        }else
            return \Response::json(['msg'=> 'Le model n\'est pa renseigne dans cette requette', 500]);
    }

    public function update(AllRequest $request, $id)
    {

        $jours = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];

        $classe_id = !empty($request->get('classe_id')) ? Classe::where('ministere', $request->get('classe_id'))->get()->first()->id : 1;

        $jour = !empty($request->get('jour')) ? $request->get('jour') : 0;

        $club = !empty($request->get('club')) ? $request->get('club') : 'Aventurier';

        if(!empty($request->get('model'))){
                   
            $model = 'App\Models\\'.ucfirst($request->get('model'));

            $temp=[];

            $instance = strtolower($request->get('model'));

            switch ($instance) {
                case 'ecosa':
                    $data = $request->all();
                    $data['classe_id'] = $classe_id;
                    $temp = $model::where('id', $id)
                    ->where('classe_id', $classe_id)->get()->first();
                    //return \Response::json(['message'=> $temp], 200);
                    $temp->update($data);
                    break;
                case 'laic':
                    $data = $request->all();
                    $data['classe_id'] = $classe_id;
                    $temp = $model::where('id', $id)
                    ->where('classe_id', $classe_id)->get()->first();
                    $temp->update($data);
                    break;
                case 'culte':
                    $data = $request->all();
                    $data['classe_id'] = $classe_id;
                    $temp = $model::where('id', $id)
                    ->where('classe_id', $classe_id)->get()->first();
                    $temp->update($data);
                    break;
                case 'anciennat':
                    $temp = $model::where('id', $id)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'musique':
                    $temp = $model::where('id', $id)->get()->first();
                    $temp->update($request->all());                    
                    break;
                case 'reunion':
                    $temp = $model::where('id', $id)->get()->first();
                    $data = $request->all();
                    $data['jour'] = (array_search(ucfirst(strtolower($jour)), $jours) + 1);
                    //return \Response::json(['message'=> $data], 200);
                    $temp->update($data);
                case 'diaconat':
                    $temp = $model::where('id', $id)->get()->first();
                    $data = $request->all();
                    $data['jour'] = (array_search(ucfirst(strtolower($jour)), $jours) + 1);
                    //return \Response::json(['message'=> $data], 200);
                    $temp->update($data);
                    break;
                case 'etudes':
                    $temp = $model::where('id', $id)->get()->first();
                    $data = $request->all();
                    $data['jour'] = (array_search(ucfirst(strtolower($jour)), $jours) + 1);
                    $temp->update($data);
                    break;
                case 'jeunesse':
                    $temp = $model::where('id', $id)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'divers':
                    $temp = $model::where('id', $id)->get()->first();
                    $temp->update($request->all());
                    break;
                case 'annonce':
                    $annonce = Annonce::findOrFail($id)->first();
                    $annonce->statut = 0;
                    $annonce->pense = $request->get('pense');
                    $annonce->pour_le = $request->get('pour_le');
                    $annonce->save();
                    Iteration::where('id', $id)->update(['status'=> '0', 'changed'=> '1']);
                    break;
                default:
                    Iteration::where('pour_le', $pour_le)->update(['changed'=> '0']);
                    $reponse = ['status'=> '200', 'msg'=> 'Departement '.$instance.'non retrouver'];
                    return \Response::json($reponse, 402);
                    break;
            }

            $annonce = Annonce::where('state', 'active')->get()->first();

            Iteration::where('eglise_id', $annonce->eglise_id)
                ->where('pour_le', $annonce->pour_le)
                ->get()
                ->last()
                ->update([
                    'changed'=> '0'
                ]);

            $annonce->statut = 0;

            $annonce->save();
            
            $reponse = ['message'=> 'Modification reussie', 'data'=>$temp];

            return \Response::json($reponse, 200);   
            
        }else
            return \Response::json(['status'=> '404', 'message'=>'Departement non retrouver', 'request'=> $request->all()], 404);

    }

    public function listPublish()
    {
        $temp = Annonce::where('eglise_id', \Auth()->user()->eglise_id)->where('statut', '0')->get();

        $response = ['annonces'=> $temp];

        return \Response::json($response, 200);
    }

    public function publish(AllRequest $request, $id)
    {

        $annonce = Annonce::findOrFail($id);

        $annonce->statut = 1;

        $disponible = $request->get('statut');

        $eglise_id = $request->user()->eglise_id;

        $pour_le = $request->get('pour_le');

        if($annonce->state == 'soon')
        {
            
            Iteration::create([
                'pour_le'=> $pour_le,
                'eglise_id'=> $eglise_id,
                'status'=> '1',
                'changed'=> '0',
            ]);
        
        }else{

            Iteration::where('eglise_id', $eglise_id)
                ->get()
                ->last()
                ->update([
                    'changed'=> '1'
                ]);

        }

        $annonce->state = 'active';

        $annonce->save();

        return \Response::json(['message'=> 'Publication reussie'], 200);


    }

    public function listUpdate()
    {
        $annonce = Annonce::where('state', 'soon')->orWhere('state', 'active')->get();

        if($annonce->first())
            $reponse = ['annonces'=> $annonce, 'message'=> 'operation reussie'];
        else
            $reponse = ['message'=> 'operation non reussie pas de publication retrouver'];

        return \Response::json($reponse, 200);
    }


}
