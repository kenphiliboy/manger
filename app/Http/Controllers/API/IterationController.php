<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Iteration;

class IterationController extends Controller
{

	public function getIteration()
	{
		$eglise_id = \Auth::user()->eglise_id;

		return Response()->json(Iteration::where('status', '1')->where('eglise_id', $eglise_id)->get()->last(), 200);
	}


}
