<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Eglise;


class AuthController extends Controller
{
    private $successStatus = 201;

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        
        $request->validate([
            'password' => 'required|string|min:4',
            'telephone' => 'required|string|max:255|',
            'email' => 'required|string|email|max:255',
        ]);

        $input = $request->all(); 
        
        $input['password'] = bcrypt($input['password']); 
        
        $user = User::create($input);

        $tokenResult = $user->createToken('Personal Access Token');
        
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'message'=> 'inscriotn reussie',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 201);
    
    }

    public function updateProfil(Request $request)
    {
        $request->validate([
            'nom_complet' => 'required|string|max:255',
            'telephone' => 'required|string|max:255|',
            'email' => 'required|string|email|max:255|',
            'genre' => 'required|string|max:10',
            'naissance' => 'required|date',
            
        ]);

        $input = $request->all();

        $user = $request->user();

        $user->nom_complet = $input['nom_complet'];
        $user->telephone = $input['telephone'];
        $user->email = $input['email'];
        $user->genre = $input['genre'];
        $user->naissance = $input['naissance'];

        $user->save();

        return response()->json(['massage'=> 'Mise a jour complete',
                                'users'=>$user], 200);

    }

    public function update_password(Request $request){

        $request->validate([
            'old_p'=> 'required|string',
            'password'=> 'required|string|min:6',
            
        ]);

        $input = $request->all();

        $user = $request->user();

        if(\Hash::check($input['old_p'], $user->password)){

            $user->password = bcrypt($input['password']);

            $user = $user->save();

            $user = $request->user();

            $tokenResult = $user->createToken('Personal Access Token');
        
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

            return response()->json([
                'message'=> 'Mise a jour reussie',
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ], 201);
        
        }else{

            return response()->json([
                'message'=> 'Mot de passe incorrect',
                'access_token' => 'null',
                'token_type' => 'null',
                'expires_at' => 'null'
            ], 401 );

        }

    }

    public function newToken(Request $request)
    {
        
        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

            return response()->json([
                'message'=> 'Mise du token a jour reussie',
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'aucun compte n\'est enregistre avec ces identifiants'
            ], 401);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        //dd(\Session::get('Tokens'));
        return response()->json($request->user());
    }


    public function getDataRegister(Request $request)
    {
        $request->validate([
            'password' => 'required|string',
            'telephone' => 'required|string|max:255|',
            'email' => 'required|string|email|max:255',
        ]);
        

        return str_replace('/&quot;/g', '"', Eglise::all());
    }

}