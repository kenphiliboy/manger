<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ {
		Iteration,
		Classe,
		Ecossa,
		Laic,
		Cult,
		Reunion,
		Diacre,
		Jeunesse,
		Musique,
		Divers,
		Ancienna,
		Annonce,
		Groupe,
        Distrie,
        Eglise,
	};
use Response;


class ApiController extends Controller
{
	public function getUser()
	{
		return \Response()->json(auth()->user());
	}

    public function getIteration()
    {
        return str_replace('/&quot;/g', '"', Iteration::all()->last());
    }

    public function getAllIteration()
    {    	
    	return str_replace('/&quot;/g', '"', Iteration::all());
    }

    public function getAnnonce()
    {
        $annonce = Annonce::all()->last();

    	//return  str_replace('/&quot;/g', '"', );
    }

    public function getAllAnnonce()
    {
        $models = ["Classe","Ancienna","Diacre","Ecossa","Laic","Cult","Jeunesse","Musique","Reunion","Divers","Groupe"];
        
        $data = []; 
        
        foreach ($models as $key => $model) {
            if($model === "Classe"){
                $model1 = "App\Models\\".$model;
                $data[$model] = $model1::all();                
            }else{
                $model1 = "App\Models\\".$model;
                $data[$model] = $model1::all()->last();
            }
        }
        
        $annonce = Annonce::all()->last();
 
        /*\DB::table('ecossas')
                    ->leftJoin('classes', 'classe_id', '=', 'classes.id')
                    ->get();*/  
        
        $annonce->data = json_encode($data, true);

    	return str_replace('/&quot;/g', '"', $annonce);
    }

    public function getAnnonceByDate($from, $to)
    {
        return str_replace('/&quot;/g', '"', Annonce::whereBetween('pour_le', [$from, $to])->get());
    }

    public function getDataRegister(Request $request)
    {
        $validator = \Validator::make($request->all(), [ 
            'telephone' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 

        ]);
        
        if ($validator->fails())
            return response()->json(['errors'=>$validator->errors(), 
                                    'message'=>'tous les champs sont obligatoires'],
                                     422);

        return str_replace('/&quot;/g', '"', Eglise::all());
    }



}
