<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth')->only(['home']);
    	//$this->middleware('auth:eglise');
    }

    public function index()
    {
    	return view('home');
    }

    public function log()
    {
    	return view('welcome');
    }

    public function download()
    {
         $file = public_path(). "/download/annonce.apk";

        $headers = array(
              'Content-Type: application/apk',
            );

        return \Response::download($file);
    }

}
