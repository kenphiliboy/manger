<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = [
    	'annonce_id', 'user_id', 'eglise_id', 'district_id', 'departement', 'message'
    ];
}
