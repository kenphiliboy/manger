<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    
    protected $fillable = [
			    	    	'eglise_id',
			    	    	'pour_le',
			    	    	'jour',
			    	    	'bienvenue',
			    	    	'remerciement',
			    	    	'meditation',
			    	    ];
}
