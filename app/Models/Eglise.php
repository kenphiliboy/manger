<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\User;
use App\Models\Distrie;

class Eglise extends Authenticatable
{

    use Notifiable;

    protected $guard = 'eglise';

    private $countUser;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'adresse', 'status', 'localisation', 'user_id', 'user_login', 'password','distrie_id',
    ];

    public function getCountUserAttribute()
    {
        return User::where('eglise_id', $this->id)->get()->count();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function distrie()
    {
        return $this->belongsTo(Distrie::class);
    }

    public function getGuardAttribute()
    {
        return $this->guard;
    }




}
