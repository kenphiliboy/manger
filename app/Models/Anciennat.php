<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anciennat extends Model
{
    protected $fillable = [
		    	    	'eglise_id',
		    	    	'pour_le',
		    	    	'liste'
		    	    ];
}
