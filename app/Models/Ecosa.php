<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ecosa extends Model
{
    
    protected $fillable = [
				    	    'eglise_id', 'classe_id', 'pour_le', 'directeur_trice', 'priere', 'rapport', '5minutes', 'bulletin',
					    	];
}
