<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Iteration extends Model
{
    protected $fillable = ['status', 'pour_le', 'changed', 'eglise_id'];
}
