<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etudes extends Model
{
    protected $fillable = [
        'eglise_id', 'jour', 'avec','pour_le',
    ];
}
