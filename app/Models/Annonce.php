<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ {
        Iteration,
        Classe,
        Ecossa,
        Laic,
        Cult,
        Reunion,
        Diacre,
        Jeunesse,
        Musique,
        Divers,
        Ancienna,
        Annonce,
        Groupe,
    };

class Annonce extends Model
{
	private $data;



    protected $fillable = [
    	'pour_le', 'pense', 'eglise_id','statut'
    ];

    
}
