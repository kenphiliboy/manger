<?php

namespace App\Models;

use App\Models\Eglise;
use Illuminate\Database\Eloquent\Model;

class Distrie extends Model
{

    protected $fillable = ['user_id', 'nom', 'secteur', 'responsable', 'partie_de', 'eglise_mere',];

    public function eglise()
    {
    	return $this->hasMany(Eglise::class);
    }

    


}
