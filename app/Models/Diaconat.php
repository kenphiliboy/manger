<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diaconat extends Model
{
    protected $fillable = [
			    	    	'eglise_id',
			    	    	'pour_le',		    	    	
			    	    	'groupe',
			    	    	'jour',
			    	    	'diacres_de_service',
			    	    ];
}
