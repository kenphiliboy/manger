<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Musique extends Model
{
    
    protected $fillable = [
			    	    	'eglise_id', 'pour_le', 'groupe', 'chants_speciaux','musicien'
			    	    ];
}
