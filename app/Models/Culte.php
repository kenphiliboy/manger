<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Culte extends Model
{
    protected $fillable = [

			    	    	'eglise_id',
			    	    	'classe_id',
			    	    	'pour_le',
			    	    	'bienvenue',
			    	    	'appel',
			    	    	'lecture',
			    	    	'priere_pastorale',
			    	    	'service_de_fidelite',
			    	    	'predication',
			    	    	'benediction',
			    	    ];
}
