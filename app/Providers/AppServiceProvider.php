<?php

namespace App\Providers;

use Laravel\Passport\Passport; 
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->registerPolicies();

        Passport::routes();

        Blade::if('admin', function () {
            return auth()->check() && (auth()->user()->admin || auth()->user()->root);
        });//pour l'utilisastion de l'@admin avec blade 

        Blade::if('root', function (){
            return auth()->check() && (auth()->user()->root);
        });

        Blade::if('eglise', function(){
            return auth()->check() && (auth()->user()->guard === "eglise");
        });

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
